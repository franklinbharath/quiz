/**
 * 
 */
package fr.epita.quiz.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.constants.QueryConstants;
import fr.epita.quiz.datamodel.Answer;
import fr.epita.quiz.datamodel.MCQOption;
import fr.epita.quiz.datamodel.MCQuestion;
import fr.epita.quiz.datamodel.OpenQuestion;
import fr.epita.quiz.datamodel.Question;
import fr.epita.quiz.datamodel.Topic;
import fr.epita.quiz.logger.Logger;
import fr.epita.quiz.tables.AnswerTable;
import fr.epita.quiz.tables.MCQOptionTable;
import fr.epita.quiz.tables.QuestionTable;
import fr.epita.quiz.tables.QuestionTopicMapper;
import fr.epita.quiz.tables.TopicTable;

// TODO: Auto-generated Javadoc
/**
 * The Class QuestionDAO has all the CRUD operation with respect to questions.
 *
 * @author bharath
 */
public class QuestionDAO {
	
	/**
	 * Creates a MCQuestion in database.
	 *
	 * @param question the mcq question
	 */
	public void createMCQuestion(MCQuestion question) {
		Map<String,String> resp = createQuestion(question);
		if(resp.get(Constants.STATUS).equals(Constants.SUCCESS)) {
			Integer questionId = Integer.valueOf(resp.get(QuestionTable.ID));
			question.setId(questionId);
			addMCQOptions(question.getOptions(),questionId);
		}
	}
	
	/**
	 * Adds MC options for given question in the database.
	 *
	 * @param options the mcq options
	 * @param questionId the question id
	 */
	public void addMCQOptions(List<MCQOption> options,int questionId) {
		List<String> colNames = new ArrayList<>();
		colNames.add(MCQOptionTable.OPTION);
		colNames.add(MCQOptionTable.QUESTIONID);
		colNames.add(MCQOptionTable.VALID);
		String insertQuery = ConstructQuery.constructInsertQuery(MCQOptionTable.TABLENAME,colNames);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			for (MCQOption option : options) {
				insertStatement.setString(1, option.getOption());
				insertStatement.setInt(2, questionId);
				insertStatement.setBoolean(3, option.isValid());
				insertStatement.addBatch();
			}
			insertStatement.executeBatch();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Creates a Open Question in database.
	 *
	 * @param question the question
	 */
	public void createOpenQuestion(OpenQuestion question) {
		Map<String,String> resp = createQuestion(question);
		if(resp.get(Constants.STATUS).equals(Constants.SUCCESS)) {
			Integer questionId = Integer.valueOf(resp.get(QuestionTable.ID));
			question.setId(questionId);
			addAnswerForQuestion(question);
		}
	}
	
	/**
	 * Adds answer for given question in database.
	 *
	 * @param question the question
	 */
	public void addAnswerForQuestion(OpenQuestion question) {
		List<String> colNames = new ArrayList<>();
		colNames.add(AnswerTable.ANSWER);
		colNames.add(AnswerTable.QUESTIONID);
		String insertQuery = ConstructQuery.constructInsertQuery(AnswerTable.TABLENAME,colNames);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			Answer ans = question.getAnswer();
			insertStatement.setString(1, ans.getAnswer());
			insertStatement.setInt(2, question.getId());
			insertStatement.execute();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Checks if question is already present to avoid duplication and adds question to database.
	 *
	 * @param question the question
	 * @return Map
	 */
	public Map<String,String> createQuestion(Question question) {
		//check if the question already exists in the database
		Map<String,String> resp = new HashMap<>();
		String condition = ConstructQuery.setCriteria(QuestionTable.QUESTION, QueryConstants.EQUAL,true);
		List<String> colNames = new ArrayList<>();
		colNames.add(QuestionTable.QUESTION);
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
			PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
			preparedStmt.setString(1,question.getQuestion());
		    try(ResultSet result = preparedStmt.executeQuery();){
		    	if(!result.next()) {
		    		Integer questionId = addQuestionToDB(question,connection);
		    		resp.put(Constants.STATUS, Constants.SUCCESS);
		    		resp.put(QuestionTable.ID, questionId.toString());
		    		resp.put(Constants.MSG, "Question created successfully");
		    	}else {
		    		resp.put(Constants.STATUS, Constants.FAILED);
		    		resp.put(Constants.MSG, "Question already exists");
		    	} 
		    	return resp;
		    }catch(SQLException e) {
		    	Logger.logMessage(e);
		    }
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		resp.put(Constants.STATUS, Constants.FAILED);
		resp.put(Constants.MSG, "Unkown Error");
		return resp;
	}
	
	/**
	 * Add question to database.
	 *
	 * @param question the question
	 * @param connection the connection
	 * @return Integer
	 */
	public Integer addQuestionToDB(Question question,Connection connection) {
		List<String> colNames = new ArrayList<>();
		colNames.add(QuestionTable.QUESTION);
		colNames.add(QuestionTable.DIFFICULTY);
		colNames.add(QuestionTable.TYPE);
		String insertQuery = ConstructQuery.constructInsertQuery(QuestionTable.TABLENAME,colNames);
		try (PreparedStatement insertStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS);){
			insertStatement.setString(1, question.getQuestion());
			insertStatement.setInt(2, question.getDifficulty());
			insertStatement.setInt(3, question.getType());
			int rowAffected = insertStatement.executeUpdate();
			if(rowAffected == 1)
			{
				try(ResultSet rs = insertStatement.getGeneratedKeys();){
					rs.next();
					int id = rs.getInt(QuestionTable.ID);
					question.setId(id);
					Map<String,Integer> topicMap= addTopics(question,connection);
					mapTopicsToQuestions(question,topicMap,connection);
					return id;
				}catch(SQLException e) {
					Logger.logMessage(e);
				}
			}
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
		return null;
	}
	
	/**
	 * Checks if topics are already present in database to avoid duplication and adds it to database.
	 *
	 * @param question the question
	 * @param connection the connection
	 * @return Map
	 */
	public Map<String,Integer> addTopics(Question question,Connection connection) {
		//check if the topics are already added in DB
		//if not added then call addTopicToDB method to add new topics to DB
		List<String> topics = question.getTopics();
		Map<String,Integer> topicMap = new HashMap<>();
		int len = topics.size();
		String condition = ConstructQuery.setInCriteriaClause(TopicTable.TOPIC,len,true,true);
		List<String> colNames = new ArrayList<>();
		colNames.add("*");
		String selectQuery = ConstructQuery.constructSelectQuery(TopicTable.TABLENAME,colNames,condition);
		try(PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
			for(int i=1;i<=len;i++) {
				preparedStmt.setString(i, topics.get(i-1).toUpperCase());
			}
			try(ResultSet result = preparedStmt.executeQuery();){
				while(result.next()) {
					topicMap.put(result.getString(TopicTable.TOPIC).toUpperCase(), result.getInt(TopicTable.ID));
				}
				if(topicMap.size()<topics.size()) {
					topicMap = addTopicToDB(topics,topicMap,connection);
				}
			}catch (SQLException e) {
				Logger.logMessage(e);
			}
		} catch (SQLException e) {
			Logger.logMessage(e);
		}
		return topicMap;
	}
	
	/**
	 * Adds topic to database.
	 *
	 * @param topics the topics
	 * @param topicMap the topic map
	 * @param connection the connection
	 * @return Map
	 */
	public Map<String,Integer> addTopicToDB(List<String> topics,Map<String,Integer> topicMap,Connection connection) {
		List<String> colNames = new ArrayList<>();
		colNames.add(TopicTable.TOPIC);
		String insertQuery = ConstructQuery.constructInsertQuery(TopicTable.TABLENAME,colNames);
		try (PreparedStatement insertStatement = connection.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);){
			List<String> newTopicList = new ArrayList<>();
			for(String topic:topics) {
				if(!topicMap.containsKey(topic.toUpperCase())) {
					insertStatement.setString(1, topic);
					insertStatement.addBatch();
					newTopicList.add(topic);
				}
			}
			int[] ret = insertStatement.executeBatch();
			if(ret.length>0) {
				try(ResultSet rs = insertStatement.getGeneratedKeys()){
					for (String topic : newTopicList) {
						if(rs.next()) {
							topicMap.put(topic.toUpperCase(), rs.getInt(TopicTable.ID));
						}
					}
				}catch (SQLException e) {
					Logger.logMessage(e);
				}
			}
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
		return topicMap;
	}
	
	/**
	 * A map between question and topic is created in QuestionTopicMapper table.
	 *
	 * @param question the question
	 * @param topicMap the topic map
	 * @param connection the connection
	 */
	public void mapTopicsToQuestions(Question question,Map<String,Integer> topicMap,Connection connection) {
		List<String> colNames = new ArrayList<>();
		colNames.add(QuestionTopicMapper.TOPICID);
		colNames.add(QuestionTopicMapper.QUESTIONID);
		String insertQuery = ConstructQuery.constructInsertQuery(QuestionTopicMapper.TABLENAME,colNames);
		try (PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			for (Object value : topicMap.values()) {
				insertStatement.setObject(1, value);
				insertStatement.setObject(2, question.getId());
				insertStatement.addBatch();
			}
			insertStatement.executeBatch();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	// import from XML functionality starts
	/**
	 * Adds all topics in database for given questions.
	 *
	 * @param topics the topics
	 * @return Map
	 */
	public  Map<String,Integer> addAllTopicsForQuestions(Set<String> topics) {
		Map<String,Integer> topicMap = new HashMap<>();
		int len = topics.size();
		String condition = ConstructQuery.setInCriteriaClause(TopicTable.TOPIC,len,true,true);
		List<String> colNames = new ArrayList<>();
		colNames.add("*");
		String selectQuery = ConstructQuery.constructSelectQuery(TopicTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				int i=1;
				for (String topic : topics){
					preparedStmt.setString(i++, topic.toUpperCase());
				}
				try(ResultSet result = preparedStmt.executeQuery();){
					while(result.next()) {
						topicMap.put(result.getString(TopicTable.TOPIC).toUpperCase(), result.getInt(TopicTable.ID));
					}
					if(topicMap.size()<topics.size()) {
						topicMap = addTopicToDB(new ArrayList<String>(topics),topicMap,connection);
					}
				}catch (SQLException e) {
					Logger.logMessage(e);
				}
			
		} catch (SQLException e) {
			Logger.logMessage(e);
		}
		return topicMap;
	}
	
	/**
	 * Creates list of MC Question in the database.
	 *
	 * @param mcQuestions the mcq questions
	 * @param topicMap the topic map
	 */
	public void createMCQuestion(List<MCQuestion> mcQuestions, Map<String,Integer> topicMap) {
		List<Question> questionList = new ArrayList<>();
		for(MCQuestion question:mcQuestions) {
			questionList.add(question);
		}
		List<Integer> ids = addQuestionsToDB(questionList,topicMap);
		int i=0;
		for(MCQuestion question:mcQuestions) {
			question.setId(ids.get(i++));
			
		}
		addMCQOptionsToQuestion(mcQuestions);
	}
	
	/**
	 * Adds MCQ options to database.
	 *
	 * @param mcQuestions the mcq questions
	 */
	public void addMCQOptionsToQuestion(List<MCQuestion> mcQuestions) {
		List<String> colNames = new ArrayList<>();
		colNames.add(MCQOptionTable.OPTION);
		colNames.add(MCQOptionTable.QUESTIONID);
		colNames.add(MCQOptionTable.VALID);
		String insertQuery = ConstructQuery.constructInsertQuery(MCQOptionTable.TABLENAME,colNames);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			for(MCQuestion question:mcQuestions) {
				List<MCQOption> options = question.getOptions();
				for(MCQOption option:options) {
					insertStatement.setString(1, option.getOption());
					insertStatement.setInt(2, question.getId());
					insertStatement.setBoolean(3, option.isValid());
					insertStatement.addBatch();
				}
			}
			insertStatement.executeBatch();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Creates list of open questions in the database.
	 *
	 * @param openQuestions the open question
	 * @param topicMap the topic map
	 */
	public void createOpenQuestions(List<OpenQuestion> openQuestions, Map<String,Integer> topicMap) {
		List<Question> questionList = new ArrayList<>();
		for(OpenQuestion question:openQuestions) {
			questionList.add(question);
		}
		List<Integer> ids = addQuestionsToDB(questionList,topicMap);
		int i=0;
		for(OpenQuestion question:openQuestions) {
			question.setId(ids.get(i++));
		}
		addAnswerForQuestion(openQuestions);
	}
	
	/**
	 * Adds answers to database for given questions.
	 *
	 * @param openQuestions the open questions
	 */
	public void addAnswerForQuestion(List<OpenQuestion> openQuestions) {
		List<String> colNames = new ArrayList<>();
		colNames.add(AnswerTable.ANSWER);
		colNames.add(AnswerTable.QUESTIONID);
		String insertQuery = ConstructQuery.constructInsertQuery(AnswerTable.TABLENAME,colNames);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			for(OpenQuestion question:openQuestions) {
				Answer ans = question.getAnswer();
				insertStatement.setString(1, ans.getAnswer());
				insertStatement.setInt(2, question.getId());
				insertStatement.addBatch();
			}
			insertStatement.executeBatch();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Adds list of question to database.
	 *
	 * @param questions the open questions
	 * @param topicMap the topic map
	 * @return List
	 */
	public List<Integer> addQuestionsToDB(List<Question> questions,Map<String,Integer> topicMap) {
		List<String> colNames = new ArrayList<>();
		List<Integer> ids = new ArrayList<>();
		colNames.add(QuestionTable.QUESTION);
		colNames.add(QuestionTable.DIFFICULTY);
		colNames.add(QuestionTable.TYPE);
		String insertQuery = ConstructQuery.constructInsertQuery(QuestionTable.TABLENAME,colNames);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS);){
			for(Question question:questions) {
				insertStatement.setString(1, question.getQuestion());
				insertStatement.setInt(2, question.getDifficulty());
				insertStatement.setInt(3, question.getType());
				insertStatement.addBatch();
			}
			insertStatement.executeBatch();
			try(ResultSet rs = insertStatement.getGeneratedKeys();){
				for(Question question:questions) {
					rs.next();
					question.setId(rs.getInt(QuestionTable.ID));
					ids.add(rs.getInt(QuestionTable.ID));
				}
				addTopicsForQuestions(questions,topicMap,connection);
			}catch(SQLException e) {
				Logger.logMessage(e);
			}
			
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
		return ids;
	}
	
	/**
	 * Adds topics to database for given questions.
	 *
	 * @param questions the Question
	 * @param topicMap the topic map
	 * @param connection the connection
	 */
	public void addTopicsForQuestions(List<Question> questions,Map<String,Integer> topicMap,Connection connection) {
		List<String> colNames = new ArrayList<>();
		colNames.add(QuestionTopicMapper.TOPICID);
		colNames.add(QuestionTopicMapper.QUESTIONID);
		String insertQuery = ConstructQuery.constructInsertQuery(QuestionTopicMapper.TABLENAME,colNames);
		try (PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			for(Question question:questions) {
				List<String> topics = question.getTopics();
				for(String topic:topics) {
					if(topicMap.containsKey(topic.toUpperCase())) {
						insertStatement.setObject(1, topicMap.get(topic.toUpperCase()));
						insertStatement.setObject(2, question.getId());
						insertStatement.addBatch();
					}
				}
			}
			insertStatement.executeBatch();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	// import from XML functionality ends
	
	/**
	 *  Deletes all options for given question id.
	 *
	 * @param questionId the question id
	 */
	public void deleteMCQOption(int questionId) {
		deleteMCQOption(questionId, new ArrayList<Integer>());
	}
	
	/**
	 * Deletes given option for given question id.
	 *
	 * @param questionId the question id
	 * @param ids - mcq ids
	 */
	public void deleteMCQOption(int questionId,List<Integer> ids) {
		String condition = ConstructQuery.setCriteria(MCQOptionTable.QUESTIONID, QueryConstants.EQUAL);
		if(!ids.isEmpty()) {
			condition = ConstructQuery.setCondtion(condition, ConstructQuery.setInCriteriaClause(MCQOptionTable.ID, ids.size(),false), QueryConstants.AND);
		}
		String delQuery = ConstructQuery.constructDeleteQuery(MCQOptionTable.TABLENAME,condition);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement deleteStatement = connection.prepareStatement(delQuery)){
			deleteStatement.setInt(1, questionId);
			if(!ids.isEmpty()) {
				for(int i=0;i<ids.size();i++) {
					deleteStatement.setInt(i+2, ids.get(i));
				}
			}
			deleteStatement.execute();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Deletes answer from the database for given question.
	 *
	 * @param questionId the question id
	 */
	public void deleteAnswer(int questionId) {
		String condition = ConstructQuery.setCriteria(AnswerTable.QUESTIONID, QueryConstants.EQUAL);
		String delQuery = ConstructQuery.constructDeleteQuery(AnswerTable.TABLENAME,condition);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement deleteStatement = connection.prepareStatement(delQuery)){
			deleteStatement.setInt(1, questionId);
			deleteStatement.execute();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Updates Open Question to database.
	 *
	 * @param question the open question
	 */
	public void updateOpenQuestion(OpenQuestion question) {
		updateQuestionToDB(question);
		//update answer to the table
		Answer ans = question.getAnswer();
		//if equal to -1 means previously it was mcq and new row need to be added in Answer table
		//make sure mcq options are deleted from MCQOption table
		if(ans.getId() == -1) {
			deleteMCQOption(question.getId());
			addAnswerForQuestion(question);
		}else {
			// it is open question just update the value in the Answer table
			String condition = ConstructQuery.setCriteria(AnswerTable.QUESTIONID, QueryConstants.EQUAL);
			List<String> colNames = new ArrayList<>();
			colNames.add(AnswerTable.ANSWER);
			String selectQuery = ConstructQuery.constructUpdateQuery(AnswerTable.TABLENAME,colNames,condition);
			DBConnection conn = new DBConnection();
			try (Connection connection = conn.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(selectQuery)){
				updateStatement.setString(1, ans.getAnswer());
				updateStatement.setInt(2, question.getId());
				updateStatement.executeUpdate();
			}catch (SQLException e) {
				Logger.logMessage(e);
			}
		}
	}
	
	/**
	 * Updates MCQ option for a given question.
	 *
	 * @param options the options
	 * @param questionId the question id
	 */
	public void updateMCQOption(List<MCQOption> options,int questionId) {
		List<Map<String,Object>> oldOptions = new ArrayList<>();
		List<MCQOption> newoptions = new ArrayList<>();
		List<Integer> ids = new ArrayList<>();
		for(MCQOption option:options) {
			if(option.getId()!=-1) {
				Map<String,Object> map = new HashMap<>();
				map.put(MCQOptionTable.OPTION, option.getOption());
				map.put(MCQOptionTable.VALID, option.isValid());
				map.put(MCQOptionTable.ID, option.getId());
				ids.add(option.getId());
				oldOptions.add(map);
			}else {
				newoptions.add(option);
			}
		}
		deleteMCQOption(questionId,ids);
		if(!newoptions.isEmpty()) {
			addMCQOptions(newoptions,questionId);
		}
		if(!oldOptions.isEmpty()) {
			String condition = ConstructQuery.setCriteria(MCQOptionTable.ID, QueryConstants.EQUAL);
			List<String> colNames = new ArrayList<>();
			colNames.add(MCQOptionTable.OPTION);
			colNames.add(MCQOptionTable.VALID);
			String updateQuery = ConstructQuery.constructUpdateQuery(MCQOptionTable.TABLENAME,colNames,condition);
			DBConnection conn = new DBConnection();
			try (Connection connection = conn.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(updateQuery);){
				for (Map<String,Object> option : oldOptions) {
					updateStatement.setObject(1, option.get(MCQOptionTable.OPTION));
					updateStatement.setBoolean(2, Boolean.parseBoolean(option.get(MCQOptionTable.VALID).toString()));
					updateStatement.setObject(3, option.get(MCQOptionTable.ID));
					updateStatement.addBatch();
				}
				updateStatement.executeBatch();
			}catch (SQLException e) {
				Logger.logMessage(e);
			}
		}
	}
	
	/**
	 * Updates MC Question to database.
	 *
	 * @param question the mcq question
	 */
	public void updateMCQuestion(MCQuestion question) {
		updateQuestionToDB(question);
		//delete answer from Answer table if exists
		deleteAnswer(question.getId());
		updateMCQOption(question.getOptions(),question.getId());
	}
	
	/**
	 * Updates question to database.
	 *
	 * @param question the question
	 */
	public void updateQuestionToDB(Question question) {
		String condition = ConstructQuery.setCriteria(QuestionTable.ID, QueryConstants.EQUAL);
		List<String> colNames = new ArrayList<>();
		colNames.add(QuestionTable.QUESTION);
		colNames.add(QuestionTable.DIFFICULTY);
		colNames.add(QuestionTable.TYPE);
		String selectQuery = ConstructQuery.constructUpdateQuery(QuestionTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement updateStatement = connection.prepareStatement(selectQuery)){
			updateStatement.setString(1, question.getQuestion());
			updateStatement.setInt(2, question.getDifficulty());
			updateStatement.setInt(3, question.getType());
			updateStatement.setInt(4, question.getId());
			updateStatement.executeUpdate();
			//handle topics, answer as well
			updateTopicsForQuestion(question,connection);
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Updates topics for given question.
	 *
	 * @param question the question
	 * @param connection the connection
	 */
	public void updateTopicsForQuestion(Question question,Connection connection) {
		String condition = ConstructQuery.setCriteria(QuestionTopicMapper.QUESTIONID, QueryConstants.EQUAL);
		String selectQuery = ConstructQuery.constructDeleteQuery(QuestionTopicMapper.TABLENAME,condition);
		try (PreparedStatement deleteStatement = connection.prepareStatement(selectQuery)){
			deleteStatement.setInt(1, question.getId());
			deleteStatement.execute();
			Map<String,Integer> topicMap= addTopics(question,connection);
			mapTopicsToQuestions(question,topicMap,connection);
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Gets all topics stored in the database.
	 *
	 * @return List
	 */
	public List<Topic> getAllTopics() {
		List<Topic> topics = new ArrayList<>();
		String selectQuery = ConstructQuery.constructSelectQuery(TopicTable.TABLENAME);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
			PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
			try(ResultSet result = preparedStmt.executeQuery();){
				while(result.next()) {
					Topic topic = new Topic();
					topic.setId(result.getInt(TopicTable.ID));
					topic.setTopic(result.getString(TopicTable.TOPIC));
					topics.add(topic);
				}
			}catch (SQLException e) {
				Logger.logMessage(e);
			}
		} catch (SQLException e) {
			Logger.logMessage(e);
		}
		return topics;
	}
	
	/**
	 * Deletes question from the database for given question id.
	 *
	 * @param questionId the question id
	 */
	// make sure to add delete cascade in tables where question id is used as foreign key 
	public void deleteQuestion(int questionId) {
		String condition = ConstructQuery.setCriteria(QuestionTable.ID, QueryConstants.EQUAL);
		String selectQuery = ConstructQuery.constructDeleteQuery(QuestionTable.TABLENAME,condition);
		DBConnection conn = new DBConnection();
		try (Connection connection = conn.getConnection();
			PreparedStatement deleteStatement = connection.prepareStatement(selectQuery)){
			deleteStatement.setInt(1, questionId);
			deleteStatement.execute();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Gets all MCQ questions matching given question ids.
	 *
	 * @param questionIds the question ids
	 * @return List
	 */
	public List<MCQuestion> getMCQuestions(List<Integer> questionIds){
		List<MCQuestion> questionList = new ArrayList<>();
		String condition = ConstructQuery.setCriteria(QuestionTable.TYPE, QueryConstants.EQUAL);
		condition = ConstructQuery.setCondtion(condition,ConstructQuery.setInCriteriaClause(QuestionTable.TABLENAME+"."+QuestionTable.ID, questionIds.size()),QueryConstants.AND);
		List<String> selColNames = new ArrayList<>();
		selColNames.add("BR_Questions.*");
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setInt(1,Constants.MCQUESTION);
				for(int i = 0;i < questionIds.size();i++) {
					preparedStmt.setInt(i+2,questionIds.get(i));
				}
				 try(ResultSet result = preparedStmt.executeQuery();){
				    	while(result.next()) {
				    		MCQuestion ques = new MCQuestion();
				    		ques.setId(result.getInt(QuestionTable.ID));
				    		ques.setQuestion(result.getString(QuestionTable.QUESTION));
				    		ques.setDifficulty(result.getInt(QuestionTable.DIFFICULTY));
				    		if(result.getInt(QuestionTable.DIFFICULTY) == Constants.DIFFICULT) {
				    			ques.setDifficultStr(Constants.DIFFICULTSTR);
				    		}else if(result.getInt(QuestionTable.DIFFICULTY) == Constants.MEDIUM) {
				    			ques.setDifficultStr(Constants.MEDIUMSTR);
				    		}else {
				    			ques.setDifficultStr(Constants.EASYSTR);
				    		}
				    		ques.setType(result.getInt(QuestionTable.TYPE));
				    		questionList.add(ques);
				    	}
				    }catch(SQLException e) {
				    	Logger.logMessage(e);
				    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			
			}
		return questionList;
	}
	
	/**
	 * Gets all open questions matching given question ids.
	 *
	 * @param questionIds the question ids
	 * @return List
	 */
	public List<OpenQuestion> getOpenQuestions(List<Integer> questionIds){
		List<OpenQuestion> questionList = new ArrayList<>();
		String condition = ConstructQuery.setCriteria(QuestionTable.TYPE, QueryConstants.EQUAL);
		condition = ConstructQuery.setCondtion(condition,ConstructQuery.setInCriteriaClause(QuestionTable.TABLENAME+"."+QuestionTable.ID, questionIds.size()),QueryConstants.AND);
		List<String> selColNames = new ArrayList<>();
		selColNames.add("BR_Questions.*");
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setInt(1,Constants.OPENQUESTION);
				for(int i = 0;i < questionIds.size();i++) {
					preparedStmt.setInt(i+2,questionIds.get(i));
				}
				 try(ResultSet result = preparedStmt.executeQuery();){
				    	while(result.next()) {
				    		OpenQuestion ques = new OpenQuestion();
				    		ques.setId(result.getInt(QuestionTable.ID));
				    		ques.setQuestion(result.getString(QuestionTable.QUESTION));
				    		ques.setDifficulty(result.getInt(QuestionTable.DIFFICULTY));
				    		if(result.getInt(QuestionTable.DIFFICULTY) == Constants.DIFFICULT) {
				    			ques.setDifficultStr(Constants.DIFFICULTSTR);
				    		}else if(result.getInt(QuestionTable.DIFFICULTY) == Constants.MEDIUM) {
				    			ques.setDifficultStr(Constants.MEDIUMSTR);
				    		}else {
				    			ques.setDifficultStr(Constants.EASYSTR);
				    		}
				    		ques.setType(result.getInt(QuestionTable.TYPE));
				    		questionList.add(ques);
				    	}
				    }catch(SQLException e) {
				    	Logger.logMessage(e);
				    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			
			}
		return questionList;
	}
	
	/**
	 * Gets all questions in the database.
	 *
	 * @return List
	 */
	public List<Question> getAllQuestions(){
		return searchQuestions(new Question(null,null,null,null));
	}
	
	/**
	 * Gets all questions based on given question criteria.
	 *
	 * @param question the question
	 * @return List
	 */
	public List<Question> searchQuestions(Question question) {
		List<Question> questionList = new ArrayList<>();
		List<String> searchValues = new ArrayList<>();
		String condition = null;
		if(question.getQuestion()!=null && !"".equals(question.getQuestion())) {
			condition = ConstructQuery.setCriteria(QuestionTable.QUESTION, QueryConstants.LIKE,true);
			searchValues.add("%"+question.getQuestion()+"%");
		}
		if(question.getDifficulty()!=null) {
			String cri = ConstructQuery.setCriteria(QuestionTable.DIFFICULTY, QueryConstants.EQUAL);
			if(condition == null) {
				condition = cri;
			}else {
				condition = ConstructQuery.setCondtion(condition, cri, QueryConstants.AND);
			}
			searchValues.add(question.getDifficulty().toString());
		}
		List<String> topics = question.getTopics();
		if(topics != null && !topics.isEmpty()) {
			String finalCri = null;
			for (int i = 0; i < topics.size(); i++) {
				searchValues.add("%"+topics.get(i)+"%");
				String topCri = ConstructQuery.setCriteria(TopicTable.TOPIC, QueryConstants.LIKE,true);
				if(finalCri == null) {
					finalCri = topCri;
				}else {
					finalCri = ConstructQuery.setCondtion(finalCri,topCri,QueryConstants.OR);
				}
			}
			if(condition == null) {
				condition = finalCri;
			}else {
				condition = ConstructQuery.setCondtion(condition, "("+finalCri+")", QueryConstants.AND);
			}
		}
		StringBuilder join = new StringBuilder();
		join.append(ConstructQuery.constructJoin(QueryConstants.INNERJOIN, QuestionTable.TABLENAME, QuestionTable.ID, QuestionTopicMapper.TABLENAME, QuestionTopicMapper.QUESTIONID));
		join.append(ConstructQuery.constructJoin(QueryConstants.INNERJOIN, QuestionTopicMapper.TABLENAME, QuestionTopicMapper.TOPICID, TopicTable.TABLENAME, TopicTable.ID));
		List<String> selColNames = new ArrayList<>();
		selColNames.add("BR_Questions.*");
		selColNames.add("GROUP_CONCAT(BR_TOPICS .TOPIC) AS TOPICS");
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition, join.toString());
		selectQuery = selectQuery + ConstructQuery.getGroupBy(QuestionTable.TABLENAME+"."+QuestionTable.ID);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
			PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
			for(int i = 0;i < searchValues.size();i++) {
				preparedStmt.setString(i+1,searchValues.get(i));
			}
			 try(ResultSet result = preparedStmt.executeQuery();){
			    	while(result.next()) {
			    		Question ques = new Question();
			    		ques.setId(result.getInt(QuestionTable.ID));
			    		ques.setQuestion(result.getString(QuestionTable.QUESTION));
			    		ques.setDifficulty(result.getInt(QuestionTable.DIFFICULTY));
			    		if(result.getInt(QuestionTable.DIFFICULTY) == Constants.DIFFICULT) {
			    			ques.setDifficultStr(Constants.DIFFICULTSTR);
			    		}else if(result.getInt(QuestionTable.DIFFICULTY) == Constants.MEDIUM) {
			    			ques.setDifficultStr(Constants.MEDIUMSTR);
			    		}else {
			    			ques.setDifficultStr(Constants.EASYSTR);
			    		}
			    		ques.setType(result.getInt(QuestionTable.TYPE));
			    		ques.setTopics(Arrays.asList(result.getString("TOPICS").split(",")));
			    		questionList.add(ques);
			    	}
			    }catch(SQLException e) {
			    	Logger.logMessage(e);
			    }
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return questionList;
	}
	
	/**
	 * Gets list of mcq options for given question ids.
	 *
	 * @param questionIds the question ids
	 * @param withAnswer with answer
	 * @return Map
	 */
	public Map<Integer,List<MCQOption>> getOptionsForMCQuestions(List<Integer> questionIds,Boolean withAnswer) {
		Map<Integer,List<MCQOption>> map = new HashMap<>();
		String condition = ConstructQuery.setInCriteriaClause(MCQOptionTable.TABLENAME+"."+MCQOptionTable.QUESTIONID, questionIds.size());
		List<String> selColNames = new ArrayList<>();
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.OPTION);
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.ID);
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.QUESTIONID);
		if(withAnswer) {
			selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.VALID);
		}
		String selectQuery = ConstructQuery.constructSelectQuery(MCQOptionTable.TABLENAME, selColNames, condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				for(int i = 0;i < questionIds.size();i++) {
					preparedStmt.setInt(i+1,questionIds.get(i));
				}
				try(ResultSet result = preparedStmt.executeQuery();){
			    	while(result.next()) {
			    		MCQOption mcqOption = new MCQOption();
			    		mcqOption.setOption(result.getString(MCQOptionTable.OPTION));
			    		if(withAnswer) {
			    			mcqOption.setValid(result.getBoolean(MCQOptionTable.VALID));
			    		}
			    		mcqOption.setId(result.getInt(MCQOptionTable.ID));
			    		if(map.containsKey(result.getObject(MCQOptionTable.QUESTIONID))) {
			    			List<MCQOption> opt = map.get(result.getObject(MCQOptionTable.QUESTIONID));
			    			opt.add(mcqOption);
			    		}else {
			    			List<MCQOption> opt = new ArrayList<>();
			    			opt.add(mcqOption);
			    			map.put(Integer.parseInt(result.getObject(MCQOptionTable.QUESTIONID).toString()), opt);
			    		}
			    	}
				}catch(SQLException e) {
				    Logger.logMessage(e);
				}
			
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return map;
	}
	
	/**
	 * Gets map of mcq options for given question ids.
	 *
	 * @param questionIds the question ids
	 * @return Map
	 */
	public Map<Integer,Map<Integer,MCQOption>> getOptionsForMCQuestionsInMap(List<Integer> questionIds) {
		Map<Integer,Map<Integer,MCQOption>> map = new HashMap<>();
		String condition = ConstructQuery.setInCriteriaClause(MCQOptionTable.TABLENAME+"."+MCQOptionTable.QUESTIONID, questionIds.size());
		List<String> selColNames = new ArrayList<>();
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.OPTION);
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.ID);
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.QUESTIONID);
		selColNames.add(MCQOptionTable.TABLENAME+"."+MCQOptionTable.VALID);
		String selectQuery = ConstructQuery.constructSelectQuery(MCQOptionTable.TABLENAME, selColNames, condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				for(int i = 0;i < questionIds.size();i++) {
					preparedStmt.setInt(i+1,questionIds.get(i));
				}
				try(ResultSet result = preparedStmt.executeQuery();){
			    	while(result.next()) {
			    		MCQOption mcqOption = new MCQOption();
			    		mcqOption.setOption(result.getString(MCQOptionTable.OPTION));
			    		mcqOption.setValid(result.getBoolean(MCQOptionTable.VALID));
			    		mcqOption.setId(result.getInt(MCQOptionTable.ID));
			    		if(map.containsKey(result.getObject(MCQOptionTable.QUESTIONID))) {
			    			Map<Integer,MCQOption> optionMap = map.get(result.getObject(MCQOptionTable.QUESTIONID));
			    			optionMap.put(result.getInt(MCQOptionTable.ID), mcqOption);
			    		}else {
			    			Map<Integer,MCQOption> optionMap = new HashMap<>();
			    			optionMap.put(result.getInt(MCQOptionTable.ID), mcqOption);
			    			map.put(Integer.parseInt(result.getObject(MCQOptionTable.QUESTIONID).toString()), optionMap);
			    		}
			    	}
				}catch(SQLException e) {
				    Logger.logMessage(e);
				}
			
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return map;
	}
	
	/**
	 * Get multiple options for given question id.
	 *
	 * @param questionId the question id
	 * @return List
	 */
	public List<MCQOption> getOptionsForMCQuestion(int questionId) {
		List<MCQOption> mcqOptionList = new ArrayList<>();
		String condition = ConstructQuery.setCriteria(QuestionTable.TABLENAME+"."+QuestionTable.ID, QueryConstants.EQUAL);
		String join = ConstructQuery.constructJoin(QueryConstants.INNERJOIN, QuestionTable.TABLENAME, QuestionTable.ID, MCQOptionTable.TABLENAME, MCQOptionTable.QUESTIONID);
		List<String> selColNames = new ArrayList<>();
		selColNames.add(MCQOptionTable.TABLENAME+".*");
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition, join.toString());
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setInt(1,questionId);
				try(ResultSet result = preparedStmt.executeQuery();){
			    	while(result.next()) {
			    		MCQOption mcqOption = new MCQOption();
			    		mcqOption.setOption(result.getString(MCQOptionTable.OPTION));
			    		mcqOption.setValid(result.getBoolean(MCQOptionTable.VALID));
			    		mcqOption.setId(result.getInt(MCQOptionTable.ID));
			    		mcqOptionList.add(mcqOption);
			    	}
				}catch(SQLException e) {
				    Logger.logMessage(e);
				}
			
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return mcqOptionList;
	}
	
	/**
	 * Gets answer for given question id.
	 *
	 * @param questionId the question id
	 * @return Answer
	 */
	public Answer getAnwerForQuestion(int questionId) {
		String condition = ConstructQuery.setCriteria(QuestionTable.TABLENAME+"."+QuestionTable.ID, QueryConstants.EQUAL);
		String join = ConstructQuery.constructJoin(QueryConstants.INNERJOIN, QuestionTable.TABLENAME, QuestionTable.ID, AnswerTable.TABLENAME, AnswerTable.QUESTIONID);
		List<String> selColNames = new ArrayList<>();
		selColNames.add(AnswerTable.TABLENAME+".*");
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition, join.toString());
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setInt(1,questionId);
				try(ResultSet result = preparedStmt.executeQuery();){
			    	if(result.next()) {
			    		Answer ans = new Answer();
			    		ans.setAnswer(result.getString(AnswerTable.ANSWER));
			    		ans.setId(result.getInt(AnswerTable.ID));
			    		return ans;
			    	}
				}catch(SQLException e) {
				    Logger.logMessage(e);
				}
			
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return null;
	}
	
	/**
	 * Returns list of question ids for given topics, difficulty level and number of questions.
	 *
	 * @param topicIds the topic ids
	 * @param difficulty the difficulty
	 * @param limit the limit
	 * @return List
	 */
	public List<Integer> getQuestionIdsForComplexityAndTopics(List<Integer> topicIds,int difficulty,Integer limit) {
		List<Integer> ids = new ArrayList<>();
		String condition = ConstructQuery.setCriteria(QuestionTable.TABLENAME+"."+QuestionTable.DIFFICULTY, QueryConstants.EQUAL);
		condition = ConstructQuery.setCondtion(condition,ConstructQuery.setInCriteriaClause(TopicTable.TABLENAME+"."+TopicTable.ID, topicIds.size()),QueryConstants.AND);
		List<String> selColNames = new ArrayList<>();
		selColNames.add("DISTINCT("+QuestionTable.TABLENAME+"."+QuestionTable.ID+") as Ids");
		StringBuilder join = new StringBuilder();
		join.append(ConstructQuery.constructJoin(QueryConstants.INNERJOIN, QuestionTable.TABLENAME, QuestionTable.ID, QuestionTopicMapper.TABLENAME, QuestionTopicMapper.QUESTIONID));
		join.append(ConstructQuery.constructJoin(QueryConstants.INNERJOIN, QuestionTopicMapper.TABLENAME, QuestionTopicMapper.TOPICID, TopicTable.TABLENAME, TopicTable.ID));
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition, join.toString());
		if(limit!=null) {
			selectQuery+=" LIMIT "+String.valueOf(limit);
		}
		String finalQuery = "SELECT GROUP_CONCAT(Ids) as Ids FROM ("+selectQuery+")";
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
			PreparedStatement preparedStmt = connection.prepareStatement(finalQuery);){
			preparedStmt.setInt(1,difficulty);
			for(int i = 0;i < topicIds.size();i++) {
				preparedStmt.setInt(i+2,topicIds.get(i));
			}
			 try(ResultSet result = preparedStmt.executeQuery();){
			    	if(result.next() && result.getString("Ids")!=null) {
			    		String[] idArray = result.getString("Ids").split(",");
			    		for(String id:idArray) {
			    			ids.add(Integer.parseInt(id));
			    		}
			    	}
			    }catch(SQLException e) {
			    	Logger.logMessage(e);
			    }
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return ids;
	}
	
	/**
	 * For given question ids corresponding complexity level is returned.
	 *
	 * @param questionIds the question ids
	 * @return List
	 */
	public List<Map<Integer,Integer>> getDifficultyLevelForQuestions(List<Integer> questionIds) {
		List<Map<Integer,Integer>> mapList = new ArrayList<>();
		String condition = ConstructQuery.setInCriteriaClause(QuestionTable.TABLENAME+"."+QuestionTable.ID, questionIds.size());
		List<String> selColNames = new ArrayList<>();
		selColNames.add(QuestionTable.TABLENAME+"."+QuestionTable.ID);
		selColNames.add(QuestionTable.TABLENAME+"."+QuestionTable.DIFFICULTY);
		String selectQuery = ConstructQuery.constructSelectQuery(QuestionTable.TABLENAME, selColNames, condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				for(int i = 0;i < questionIds.size();i++) {
					preparedStmt.setInt(i+1,questionIds.get(i));
				}
				 try(ResultSet result = preparedStmt.executeQuery();){
				    	if(result.next()) {
				    		Map<Integer,Integer> map = new HashMap<>();
				    		map.put(result.getInt(QuestionTable.ID), result.getInt(QuestionTable.DIFFICULTY));
				    		mapList.add(map);
				    	}
				    }catch(SQLException e) {
				    	Logger.logMessage(e);
				    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			}
		return mapList;
	}
}
