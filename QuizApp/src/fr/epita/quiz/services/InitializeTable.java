/**
 * 
 */
package fr.epita.quiz.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import fr.epita.quiz.logger.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class InitializeTable is used to initialize table if is not created yet.
 *
 * @author rishi
 */
public class InitializeTable {
	
	/**
	 *  Add required table to database if not created.
	 */
	public void addTablesToDB() {
		try {
			Configuration conf = Configuration.getInstance();
			File file = new File(conf.getConfigurationValue("sql"));
			try(BufferedReader br = new BufferedReader(new FileReader(file));){
				String st; 
				DBConnection conn = new DBConnection();
				try (Connection connection = conn.getConnection();
						Statement createStatement = connection.createStatement();){
						while ((st = br.readLine()) != null) {
							createStatement.addBatch(st.trim());
						}
						createStatement.executeBatch();
				} catch (SQLException e) {
					Logger.logMessage(e);
				}
				  
			} 
		} catch (IOException  e) {
			Logger.logMessage(e);
		}
	}
}
