/**
 * 
 */
package fr.epita.quiz.services;

import java.util.ArrayList;
import java.util.List;

import fr.epita.quiz.constants.QueryConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstructQuery generates four basic SQL queries insert, select, delete and update.
 *
 * @author rishi
 */
public class ConstructQuery {
	
	/**
	 * Generates insert query for given table and columns.
	 *
	 * @param tableName the table name
	 * @param columnNames the column names
	 * @return the insert query
	 */
	public static String constructInsertQuery(String tableName, List<String> columnNames) {
		StringBuilder query = new StringBuilder("INSERT INTO ").append(tableName).append(" (")
				.append(String.join(", ", columnNames)).append(") VALUES (");
		int len = columnNames.size();
		for(int i=0;i<len;i++) {
			query.append((i!=len-1)?"?,":"?");
		}
		query.append(")");
		return query.toString();
	}
	
	/**
	 * Generates update query for given table, columns and conditions.
	 *
	 * @param tableName the table name
	 * @param columnNames the column names
	 * @param conditions the conditions
	 * @return the update query
	 */
	public static String constructUpdateQuery(String tableName, List<String> columnNames,String conditions) {
		StringBuilder query = new StringBuilder("UPDATE ").append(tableName).append(" SET ");
		int len = columnNames.size();
		for(int i=0;i<len;i++) {
			query.append(setCriteria(columnNames.get(i),QueryConstants.EQUAL));
			query.append((i!=len-1)?",":"");
		}
		query.append(" WHERE (").append(conditions).append(")");
		return query.toString();
	}
	
	/**
	 * Generates delete query for given table and conditions.
	 *
	 * @param tableName the table name
	 * @param conditions the conditions
	 * @return the delete query
	 */
	public static String constructDeleteQuery(String tableName, String conditions) {
		StringBuilder query = new StringBuilder("DELETE FROM ").append(tableName);
		if(conditions!=null) {
			query.append(" WHERE (").append(conditions).append(")");
		}
		return query.toString();
	}
	
	/**
	 * Generates select query for given table including all columns from the table.
	 *
	 * @param tableName the table name
	 * @return the select query
	 */
	public static String constructSelectQuery(String tableName) {
		List<String> selColName = new ArrayList<>();
		selColName.add("*");
		return constructSelectQuery(tableName,selColName,null);
	}
	
	/**
	 * Generates select query for given columns and conditions.
	 *
	 * @param tableName the table name
	 * @param selColNames the selected columns
	 * @param conditions the conditions
	 * @return the select query
	 */
	public static String constructSelectQuery(String tableName,List<String> selColNames, String conditions) {
		return constructSelectQuery(tableName,selColNames,conditions,null);
	}
	
	/**
	 * Generates select query for given columns and conditions along with join.
	 *
	 * @param tableName the table name
	 * @param selColNames the selected columns
	 * @param conditions the conditions
	 * @param joinTables the join tables
	 * @return the select query
	 */
	public static String constructSelectQuery(String tableName,List<String> selColNames, String conditions,String joinTables) {
		StringBuilder query = new StringBuilder("SELECT ").append(String.join(", ", selColNames)).append(" FROM ").append(tableName);
		if(joinTables!=null) {
			query.append(joinTables);
		}
		if(conditions!=null) {
			query.append(" WHERE (").append(conditions).append(")");
		}
		return query.toString();
	}
	
	/**
	 * Generates join for given table name and their corresponding column.
	 *
	 * @param joinType the join type
	 * @param baseTable base table
	 * @param baseColumn the base column
	 * @param deriveTable the derive table
	 * @param deriveColumn the derive column
	 * @return the join
	 */
	public static String constructJoin(int joinType,String baseTable,String baseColumn,String deriveTable,String deriveColumn) {
		StringBuilder join = new StringBuilder();
		if(joinType == QueryConstants.LEFTJOIN) {
			join.append(" LEFT JOIN ");
		}else {
			join.append(" INNER JOIN ");
		}
		join.append(deriveTable).append(" ON ").append(baseTable).append(".").append(baseColumn);
		join.append("=").append(deriveTable).append(".").append(deriveColumn);
		return join.toString();
	}
	
	/**
	 *  
	 * Generates group by clause for given column name.
	 *
	 * @param columnName the column name
	 * @return the group by
	 */
	public static String getGroupBy(String columnName) {
		return " GROUP BY "+columnName;
	}
	
	/**
	 * Generates criteria for given status ie like,equal,not equal.
	 *
	 * @param columnName the column name
	 * @param status the status
	 * @return the criteria
	 */
	public static String setCriteria(String columnName, int status) {
		return setCriteria(columnName,status,false);
	}
	
	/**
	 * Generate criteria for given status ie like,equal,not equal along with case insensitive.
	 *
	 * @param columnName the column name
	 * @param status the status
	 * @param caseInsenstive the case insensitive
	 * @return the criteria
	 */
	public static String setCriteria(String columnName, int status,boolean caseInsenstive) {
		switch(status) {
			case QueryConstants.LIKE:
				return caseInsenstive?" UPPER("+columnName+") LIKE UPPER(?)":columnName +" LIKE ? ";
			case QueryConstants.EQUAL:
				return caseInsenstive?" UPPER("+columnName+") = UPPER(?)":columnName +" = ? ";
			case QueryConstants.NOTEQUAL:
				return caseInsenstive?" UPPER("+columnName+")! = UPPER(?)":columnName +" != ? ";
			default:
				return null;
		}
	}
	
	/**
	 * Generates criteria for IN clause.
	 *
	 * @param columnName the column name
	 * @param count the count
	 * @return the IN clause criteria
	 */
	public static String setInCriteriaClause(String columnName,Integer count) {
		return setInCriteriaClause(columnName,count,true);
	}
	
	/**
	 * Generates criteria for IN or NOT IN clause.
	 *
	 * @param columnName the column name
	 * @param count the count
	 * @param in the in
	 * @return the IN clause criteria
	 */
	public static String setInCriteriaClause(String columnName,Integer count,Boolean in) {
		return setInCriteriaClause(columnName,count,in,false);
	}
	
	/**
	 * Generates criteria for IN or NOT IN clause along with case insensitive.
	 *
	 * @param columnName the column name
	 * @param count the count
	 * @param in the in
	 * @param upper the upper
	 * @return the IN or NOT IN clause criteria
	 */
	public static String setInCriteriaClause(String columnName,Integer count,Boolean in,Boolean upper) {
		StringBuilder builder = new StringBuilder();
		if(upper) {
			builder.append("UPPER(").append(columnName).append(")");
		}else {
			builder.append(columnName);
		}
		if(in) {
			builder.append(" IN (");
		}else {
			builder.append(" NOT IN (");
		}
		for(int i=0;i<count;i++) {
			builder.append((i!=count-1)?"?,":"?");
		}
		builder.append(")");
		return builder.toString();
	}
	
	/**
	 * Generates condition for two given columns.
	 *
	 * @param condition the condition
	 * @param condition1 the condition 1
	 * @param status the status
	 * @return the condition
	 */
	public static String setCondtion(String condition,String condition1, int status) {
		switch(status) {
			case QueryConstants.AND:
				return condition +" AND " + condition1;
			case QueryConstants.OR:
				return condition +" OR " + condition1;
			default:
				return null;
		}
	}
}
