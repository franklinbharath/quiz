/**
 * 
 */
package fr.epita.quiz.services;

import org.mindrot.jbcrypt.BCrypt;

// TODO: Auto-generated Javadoc
/**
 * The Class HashPassword hashes the given password.
 *
 * @author rishi
 */
public class HashPassword {
	
	/** The workload. */
	private static int workload = 12;
	
	/**
	 * Gets hash of given password.
	 *
	 * @param password the password
	 * @return the hash password
	 */
	public static String hashPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt(workload));
	}
}
