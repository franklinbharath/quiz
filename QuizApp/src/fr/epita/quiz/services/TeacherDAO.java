/**
 * 
 */
package fr.epita.quiz.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mindrot.jbcrypt.BCrypt;

import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.constants.QueryConstants;
import fr.epita.quiz.datamodel.Teacher;
import fr.epita.quiz.logger.Logger;
import fr.epita.quiz.tables.TeacherTable;

// TODO: Auto-generated Javadoc
/**
 * The Class TeacherDAO is used to add new teacher and to validate credentials.
 *
 * @author rishi
 */
public class TeacherDAO {
	
	/**
	 * Creates new teacher, username and hashed password is stored in database.
	 *
	 * @param teacher the teacher
	 * @return Map
	 */
	public Map<String,Object> createTeacher(Teacher teacher) {
		Map<String,Object> resp = new HashMap<>();
		if(!teachertExists(teacher)) {
			List<String> colNames = new ArrayList<>();
			colNames.add(TeacherTable.USERNAME);
			colNames.add(TeacherTable.PASSWORD);
			String insertQuery = ConstructQuery.constructInsertQuery(TeacherTable.TABLENAME,colNames);
			DBConnection conn = new DBConnection();
			try (Connection connection = conn.getConnection();
					PreparedStatement insertStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS);) {
				insertStatement.setString(1, teacher.getUsername());
				insertStatement.setString(2, teacher.hashPassword());
				insertStatement.execute();
				try(ResultSet rs = insertStatement.getGeneratedKeys();){
					if(rs.next()) {
						teacher.setId(rs.getInt(TeacherTable.ID));
					}
				}catch(SQLException e) {
					Logger.logMessage(e);
				}
				resp.put("Teacher",teacher);
				resp.put(Constants.STATUS, Constants.SUCCESS);
				return resp;
			} catch (SQLException e) {
				Logger.logMessage(e);
			}
		}else {
			resp.put(Constants.STATUS, Constants.FAILED);
			resp.put(Constants.MSG, "Username already exists");
			return resp;
		}
		resp.put(Constants.STATUS, Constants.FAILED);
		resp.put(Constants.MSG, "Unkown error");
		return resp;
	}
	
	/**
	 * Checks if username already exists in database.
	 *
	 * @param teacher the teacher
	 * @return boolean
	 */
	public boolean teachertExists(Teacher teacher) {
		String condition = ConstructQuery.setCriteria(TeacherTable.USERNAME, QueryConstants.EQUAL);
		List<String> colNames = new ArrayList<>();
		colNames.add(TeacherTable.USERNAME);
		String selectQuery = ConstructQuery.constructSelectQuery(TeacherTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setString(1,teacher.getUsername());
			    try(ResultSet result = preparedStmt.executeQuery();){
			    	return result.next();
			    }catch(SQLException e) {
			    	Logger.logMessage(e);
			    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			}
		return false;
	}
	
	/**
	 * Validates the credentials of teacher .
	 *
	 * @param teacher the teacher
	 * @return Map
	 */
	public Map<String,Object> validateTeacher(Teacher teacher) {
		Map<String,Object> resp = new HashMap<>();
		String condition = ConstructQuery.setCriteria(TeacherTable.USERNAME, QueryConstants.EQUAL);
		List<String> colNames = new ArrayList<>();
		colNames.add("*");
		String selectQuery = ConstructQuery.constructSelectQuery(TeacherTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setString(1,teacher.getUsername());
			    try(ResultSet result = preparedStmt.executeQuery();){
			    	if(result.next()) {
			    		boolean isValid = BCrypt.checkpw(teacher.getPassword(), result.getString(TeacherTable.PASSWORD));
			    		resp.put(Constants.STATUS, isValid?Constants.SUCCESS:Constants.FAILED);
			    		if(!isValid) {
			    			resp.put(Constants.MSG, "Invalid Password");
			    		}else {
			    			teacher.setId(result.getInt(TeacherTable.ID));
			    			resp.put("Teacher", teacher);
			    		}
			    		return resp;
				    }else {
				    	resp.put(Constants.STATUS, Constants.FAILED);
				    	resp.put(Constants.MSG, "Username does not exist");
				    	return resp;
				    }
			    }catch(SQLException e) {
			    	Logger.logMessage(e);
			    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			}
		resp.put(Constants.STATUS, Constants.FAILED);
		resp.put(Constants.MSG, "Unkown error");
		return resp;
	}
}
