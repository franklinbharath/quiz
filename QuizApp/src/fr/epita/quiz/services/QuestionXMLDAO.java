/**
 * 
 */
package fr.epita.quiz.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.datamodel.Answer;
import fr.epita.quiz.datamodel.MCQOption;
import fr.epita.quiz.datamodel.MCQuestion;
import fr.epita.quiz.datamodel.OpenQuestion;
import fr.epita.quiz.logger.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class QuestionXMLDAO is used for bulk addition of questions into the database.
 *
 * @author rishi
 */
public class QuestionXMLDAO {
	
	/**
	 * Parses given XML file.
	 *
	 * @param file the file
	 * @return the document
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public Document parseFile(File file) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = fact.newDocumentBuilder();
		return builder.parse(file);
	}
	
	//if a question does not match with defined xml pattern, it will be ignored
	//check questions.xml for sample
	/**
	 * Imports all question from XML file to database.
	 *
	 * @param file the xml file
	 */
	public void saveQuestionsFromXML(File file) {
		Document doc;
		Set<String> allTopics = new HashSet<>();
		List<MCQuestion> mcQuestions = new ArrayList<>();
		List<OpenQuestion> openQuestions = new ArrayList<>();
		try {
			doc = parseFile(file);
			NodeList list = doc.getElementsByTagName("question");
			for (int i = 0; i < list.getLength(); i++) {
				Element questionXML = (Element) list.item(i);
				NodeList questionNode = questionXML.getElementsByTagName("label");
				NodeList difficultyNode = questionXML.getElementsByTagName("difficulty");
				NodeList typeNode = questionXML.getElementsByTagName("type");
				// condition to check whether valid question,difficulty,type is there or not
				//if not there then ignore this question
				if(questionNode.getLength() == 0 || difficultyNode.getLength() == 0 || typeNode.getLength() == 0) {
					continue;
				}
				String questionTxt = questionNode.item(0).getTextContent(); // getting label element																					
				int difficulty = Integer.parseInt(difficultyNode.item(0).getTextContent()); // getting
				if(difficulty!=Constants.EASY && difficulty!=Constants.MEDIUM && difficulty!=Constants.DIFFICULT) {
					continue;
				}
																															
				List<String> topics = getAllTopicsFromQuestion(questionXML);																											// element
				allTopics.addAll(topics);
	
				int type = Integer.parseInt(typeNode.item(0).getTextContent());																									// text
				if(type!=Constants.MCQUESTION && type!=Constants.OPENQUESTION) {
					continue;
				}
				
				if(type==Constants.MCQUESTION) {
					List<MCQOption> options = getAllMCQOptions(questionXML);
					//atleast two option shud be present
					if(options.size()<2) {
						continue;
					}
					//atleast one option should be valid
					boolean validPresent = false;
					for(MCQOption option:options) {
						if(option.isValid()) {
							validPresent = true;
							break;
						}
					}
					if(!validPresent) {
						continue;
					}
					MCQuestion mcq = new MCQuestion();
					mcq.setQuestion(questionTxt);
					mcq.setDifficulty(difficulty);
					mcq.setTopics(topics);
					mcq.setOptions(options);
					mcq.setType(type);
					mcQuestions.add(mcq);
				}else {
					NodeList answerNode = questionXML.getElementsByTagName("answer");
					String answer = answerNode.item(0).getTextContent().trim();
					//ignore questions if does not have valid answers
					if("".equals(answer)) {
						continue;
					}
					Answer ans = new Answer();
					ans.setAnswer(answer);
					OpenQuestion open = new OpenQuestion();
					open.setAnswer(ans);
					open.setQuestion(questionTxt);
					open.setDifficulty(difficulty);
					open.setTopics(topics);
					open.setType(type);
					openQuestions.add(open);
				}
			}
			QuestionDAO dao = new QuestionDAO();
			Map<String,Integer> topicMap = dao.addAllTopicsForQuestions(allTopics);
			dao.createMCQuestion(mcQuestions, topicMap);
			dao.createOpenQuestions(openQuestions, topicMap);
		} catch (SAXException | ParserConfigurationException | IOException e) {
			Logger.logMessage(e);
		} 
	}
	
	/**
	 * Get MCQ option for a given question element.
	 *
	 * @param question the question element
	 * @return List
	 */
	private List<MCQOption> getAllMCQOptions(Element question) {
		List<MCQOption> options = new ArrayList<>();
		Element optionElement = (Element) question.getElementsByTagName("options").item(0);
		NodeList optionList = optionElement.getElementsByTagName("option");
		for(int i=0;i<optionList.getLength();i++) {
			Element optionXML = (Element) optionList.item(i);
			NodeList optiontext = optionXML.getElementsByTagName("optiontext");
			NodeList optionValue = optionXML.getElementsByTagName("valid");
			MCQOption mcqOption = new MCQOption();
			for(int j=0;j<optiontext.getLength();j++) {
				mcqOption.setOption(optiontext.item(j).getTextContent());
				mcqOption.setValid(Boolean.parseBoolean(optionValue.item(j).getTextContent()));
				options.add(mcqOption);
			}
		}
		return options;
	}
	
	/**
	 * Gets topics for a given question element.
	 *
	 * @param question the question element
	 * @return List
	 */
	private List<String> getAllTopicsFromQuestion(Element question) {
		// get element of tag name "topics"
		Element topicsElement = (Element) question.getElementsByTagName("topics").item(0);
		// extract all elements of tag name "topic" within the last
		NodeList topicsList = topicsElement.getElementsByTagName("topic");
		// init array size with lenght of list of topics
		List<String> result = new ArrayList<>();
		// loop on each topic
		for (int i = 0; i < topicsList.getLength(); i++) {
			Element topic = (Element) topicsList.item(i);
			result.add(topic.getTextContent()); // add topic text content to array
		}
		return result;
	}
}
