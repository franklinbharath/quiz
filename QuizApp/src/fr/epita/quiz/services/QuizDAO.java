/**
 * 
 */
package fr.epita.quiz.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fr.epita.quiz.constants.QueryConstants;
import fr.epita.quiz.datamodel.Answer;
import fr.epita.quiz.datamodel.Exam;
import fr.epita.quiz.datamodel.MCQOption;
import fr.epita.quiz.datamodel.MCQuestion;
import fr.epita.quiz.datamodel.OpenQuestion;
import fr.epita.quiz.datamodel.Quiz;
import fr.epita.quiz.datamodel.Student;
import fr.epita.quiz.logger.Logger;
import fr.epita.quiz.tables.QuizQuestionMapper;
import fr.epita.quiz.tables.QuizTable;

// TODO: Auto-generated Javadoc
/**
 * The Class QuizDAO generates a quiz for given question ids.
 *
 * @author bharath
 */
public class QuizDAO {
	
	/**
	 * Generates quiz for given sets of question ids.
	 *
	 * @param questionIds the question ids
	 * @param student the student
	 * @return Quiz
	 */
	public Quiz createQuiz(List<Integer> questionIds,Student student) {
		Quiz quiz = new Quiz();
		quiz.setStudent(student);
		quiz.setTitle(generateQuizTitle(student.getId()));
		QuestionDAO dao = new QuestionDAO();
		List<Integer> mcqIds = new ArrayList<>();
		List<MCQuestion> mcQuestion = dao.getMCQuestions(questionIds);
		List<OpenQuestion> openQuestion = dao.getOpenQuestions(questionIds);
		for(MCQuestion question:mcQuestion) {
			mcqIds.add(question.getId());
		}
		for(OpenQuestion question:openQuestion) {
			Answer ans = new Answer();
			question.setAnswer(ans);
		}
		if(!mcqIds.isEmpty()) {
			Map<Integer,List<MCQOption>> mcqOptions = dao.getOptionsForMCQuestions(mcqIds, false);
			for(MCQuestion question:mcQuestion) {
				question.setOptions(mcqOptions.get(question.getId()));
			}
		}
		quiz.setMcqQuestions(mcQuestion);
		quiz.setOpenQuestions(openQuestion);
		List<String> colNames = new ArrayList<>();
		colNames.add(QuizTable.TITLE);
		colNames.add(QuizTable.STUDENTID);
		DBConnection conn = new DBConnection();
		String insertQuery = ConstructQuery.constructInsertQuery(QuizTable.TABLENAME,colNames);
		try (Connection connection = conn.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS);){
			insertStatement.setString(1, quiz.getTitle());
			insertStatement.setInt(2, student.getId());
			int rowAffected = insertStatement.executeUpdate();
			if(rowAffected == 1)
			{
				try(ResultSet rs = insertStatement.getGeneratedKeys();){
					rs.next();
					int id = rs.getInt(QuizTable.ID);
					quiz.setId(id);
					mapQuestionsToQuiz(quiz,connection);
				}catch(SQLException e) {
					Logger.logMessage(e);
				}
			}
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
		return quiz;
	}
	
	/**
	 * Maps question present in quiz to QuizQuestionMapper table.
	 *
	 * @param quiz the quiz
	 * @param connection the connection
	 */
	public void mapQuestionsToQuiz(Quiz quiz,Connection connection) {
		List<String> colNames = new ArrayList<>();
		colNames.add(QuizQuestionMapper.QUIZID);
		colNames.add(QuizQuestionMapper.QUESTIONID);
		String insertQuery = ConstructQuery.constructInsertQuery(QuizQuestionMapper.TABLENAME,colNames);
		try (
			PreparedStatement insertStatement = connection.prepareStatement(insertQuery);){
			List<MCQuestion> mcQuestion = quiz.getMcqQuestions();
			List<OpenQuestion> openQuestion = quiz.getOpenQuestions();
			for(MCQuestion question:mcQuestion) {
				insertStatement.setInt(1, quiz.getId());
				insertStatement.setInt(2, question.getId());
				insertStatement.addBatch();
			}
			for(OpenQuestion question:openQuestion) {
				insertStatement.setInt(1, quiz.getId());
				insertStatement.setInt(2, question.getId());
				insertStatement.addBatch();
			}
			insertStatement.executeBatch();
		}catch (SQLException e) {
			Logger.logMessage(e);
		}
	}
	
	/**
	 * Generates Quiz title.
	 *
	 * @param studentId the student id
	 * @return String
	 */
	public String generateQuizTitle(int studentId) {
		String title = "Quiz ";
		String condition = ConstructQuery.setCriteria(QuizTable.STUDENTID, QueryConstants.EQUAL);
		List<String> colNames = new ArrayList<>();
		colNames.add("Count("+QuizTable.ID+") as Count");
		String selectQuery = ConstructQuery.constructSelectQuery(QuizTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
			PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
			preparedStmt.setInt(1,studentId);
		    try(ResultSet result = preparedStmt.executeQuery();){
		    	if(result.next()) {
		    		title+=String.valueOf(result.getInt("Count")+1);
		    	}else {
		    		title+="1";
		    	}
		    }catch(SQLException e) {
		    	Logger.logMessage(e);
		    }
		}catch(SQLException e) {
			Logger.logMessage(e);
		}
		return title;
	}
	
	/**
	 *  Evaluates quiz on completion.
	 *
	 * @param quiz the quiz
	 * @return Map
	 */
	public Exam evaluateQuiz(Quiz quiz) {
		
		Exam exam = new Exam();		
		List<MCQuestion> mcQuestion = quiz.getMcqQuestions();
		exam.setMcqQuestion(mcQuestion);
		List<OpenQuestion> openQuestion = quiz.getOpenQuestions();
		exam.setOpenQuestion(openQuestion);
		exam.setStudent(quiz.getStudent());
		List<Integer> mcqIds = new ArrayList<>();
		int score = mcQuestion.size();
		for(MCQuestion question:mcQuestion) {
			mcqIds.add(question.getId());
		}
		QuestionDAO dao = new QuestionDAO();
		List<Integer> wrongIds = new ArrayList<>();
		List<Integer> correctIds = new ArrayList<>();
		if(!mcqIds.isEmpty()) {
			Map<Integer,Map<Integer,MCQOption>> mcqOptions = dao.getOptionsForMCQuestionsInMap(mcqIds);
			for(MCQuestion question:mcQuestion) {
				if(!mcqOptions.containsKey(question.getId())) {
					continue;
				}
				Map<Integer,MCQOption> optionMap = mcqOptions.get(question.getId());
				List<MCQOption> options = question.getOptions();
				Boolean isCorrect = true;
				for(MCQOption option:options) {
					if(!optionMap.containsKey(option.getId())) {
						continue;
					}
					MCQOption mcqOption = optionMap.get(option.getId());
					if(option.isValid() != mcqOption.isValid()) {
						isCorrect = false;	
					}
					if(mcqOption.isValid()) {
						correctIds.add(option.getId());
					}
					if((option.isValid() && !mcqOption.isValid())) {
						wrongIds.add(option.getId());
					}
				}
				if(!isCorrect) {
					score-=1;
				}
			}
		}
		exam.setGrade(score);
		exam.setCorrectOptions(correctIds);
		exam.setWrongOptions(wrongIds);
		return exam;
	}
}
