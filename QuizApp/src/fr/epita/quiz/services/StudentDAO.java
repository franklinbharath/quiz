/**
 * 
 */
package fr.epita.quiz.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mindrot.jbcrypt.BCrypt;

import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.constants.QueryConstants;
import fr.epita.quiz.datamodel.Student;
import fr.epita.quiz.logger.Logger;
import fr.epita.quiz.tables.StudentTable;

// TODO: Auto-generated Javadoc
/**
 * The Class StudentDAO is used to add new student and to validate credentials.
 *
 * @author rishi
 */

//check this page after GUI

public class StudentDAO {
	
	/**
	 *  
	 * Creates new student, username and hashed password is stored in database.
	 *
	 * @param student the student
	 * @return Map
	 */
	public Map<String,Object> createStudent(Student student) {
		Map<String,Object> resp = new HashMap<>();
		if(!studentExists(student)) {
			List<String> colNames = new ArrayList<>();
			colNames.add(StudentTable.USERNAME);
			colNames.add(StudentTable.PASSWORD);
			String insertQuery = ConstructQuery.constructInsertQuery(StudentTable.TABLENAME,colNames);
			DBConnection conn = new DBConnection();
			try (Connection connection = conn.getConnection();
					PreparedStatement insertStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS);) {
				insertStatement.setString(1, student.getUsername());
				insertStatement.setString(2, student.hashPassword());
				insertStatement.executeUpdate();
				try(ResultSet rs = insertStatement.getGeneratedKeys();){
					if(rs.next()) {
						student.setId(rs.getInt(StudentTable.ID));
					}
				}catch(SQLException e) {
					Logger.logMessage(e);
				}
				resp.put("Student",student);
				resp.put(Constants.STATUS, Constants.SUCCESS);
				return resp;
			} catch (SQLException e) {
				Logger.logMessage(e);
			}
		}else {
			resp.put(Constants.STATUS, Constants.FAILED);
			resp.put(Constants.MSG, "Username already exists");
			return resp;
		}
		resp.put(Constants.STATUS, Constants.FAILED);
		resp.put(Constants.MSG, "Unkown error");
		return resp;
	}
	
	/**
	 * Checks if username already exists in database.
	 *
	 * @param student the student
	 * @return boolean
	 */
	public boolean studentExists(Student student) {
		String condition = ConstructQuery.setCriteria(StudentTable.USERNAME, QueryConstants.EQUAL);
		List<String> colNames = new ArrayList<>();
		colNames.add(StudentTable.USERNAME);
		String selectQuery = ConstructQuery.constructSelectQuery(StudentTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setString(1,student.getUsername());
			    try(ResultSet result = preparedStmt.executeQuery();){
			    	return result.next();
			    }catch(SQLException e) {
			    	Logger.logMessage(e);
			    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			}
		return false;
	}
	
	/**
	 * Validates the credentials of student .
	 *
	 * @param student the student
	 * @return the map
	 */
	public Map<String,Object> validateStudent(Student student) {
		Map<String,Object> resp = new HashMap<>();
		String condition = ConstructQuery.setCriteria(StudentTable.USERNAME, QueryConstants.EQUAL);
		List<String> colNames = new ArrayList<>();
		colNames.add("*");
		String selectQuery = ConstructQuery.constructSelectQuery(StudentTable.TABLENAME,colNames,condition);
		DBConnection conn = new DBConnection();
		try(Connection connection = conn.getConnection();
				PreparedStatement preparedStmt = connection.prepareStatement(selectQuery);){
				preparedStmt.setString(1,student.getUsername());
			    try(ResultSet result = preparedStmt.executeQuery();){
			    	if(result.next()) {
			    		boolean isValid = BCrypt.checkpw(student.getPassword(), result.getString(StudentTable.PASSWORD));
			    		resp.put(Constants.STATUS, isValid?Constants.SUCCESS:Constants.FAILED);
			    		if(!isValid) {
			    			resp.put(Constants.MSG, "Invalid Password");
			    		}else {
			    			student.setId(result.getInt(StudentTable.ID));
			    			resp.put("Student", student);
			    		}
			    		return resp;
				    }else {
				    	resp.put(Constants.STATUS, Constants.FAILED);
				    	resp.put(Constants.MSG, "Username does not exist");
				    	return resp;
				    }
			    }catch(SQLException e) {
			    	Logger.logMessage(e);
			    }
			}catch(SQLException e) {
				Logger.logMessage(e);
			}
		resp.put(Constants.STATUS, Constants.FAILED);
		resp.put(Constants.MSG, "Unkown error");
		return resp;
	}
}
