/**
 * 
 */
package fr.epita.quiz.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.datamodel.Complexity;
import fr.epita.quiz.datamodel.Topic;

// TODO: Auto-generated Javadoc
/**
 * The Class CalulateComplexity finds questions matching the required complexity.
 *
 * @author bharath
 */

public class CalulateComplexity {
	
	/** The complexity. */
	private Complexity complexity;
	
	/** The dp. */
	private boolean[][] dp; 
	
	/** The index list. */
	private List<List<Integer>> indexList = new ArrayList<>();
	
	/**
	 * Gets the complexity.
	 *
	 * @return the complexity
	 */
	public Complexity getComplexity() {
		return complexity;
	}
	
	/**
	 * Sets the complexity.
	 *
	 * @param complexity the new complexity
	 */
	public void setComplexity(Complexity complexity) {
		this.complexity = complexity;
	}
	
	/**
	 *  Calculates complexity for given quiz based on topics, difficulty and number of questions.
	 *
	 * @return question ids
	 */
	public List<Integer> getQuestionsForComplexity() {
		List<Topic> selectTopic = complexity.getSelectTopic();
		List<Integer> topicIds = new ArrayList<>();
		for(Topic topic:selectTopic) {
			topicIds.add(topic.getId());
		}
		QuestionDAO dao = new QuestionDAO();
		
		List<Integer> questionIds = dao.getQuestionIdsForComplexityAndTopics(topicIds, complexity.getDifficulty(),complexity.getTotalQuestions());
		
		//currentComplexity - for given criteria, getQuestionIdsForComplexityAndTopics method tries to fetch questions and size of the questions 
		//is multiplied with difficulty(Easy,Medium,Difficult)
		int currentComplexity = questionIds.size()*complexity.getDifficulty();
		
		//requiredComplexity - for given criteria, number of questions given by user multiplied with difficulty
		int requiredComplexity = complexity.getTotalQuestions()*complexity.getDifficulty();
		
		//currentComplexity is less than requiredComplexity we need to get questions having different difficulty level for given topic
		if(currentComplexity < requiredComplexity) {
			int[] otherDiff = new int[2];
			int i=0;
			if(complexity.getDifficulty()!=Constants.EASY) {
				otherDiff[i++]=Constants.EASY;
			}
			if(complexity.getDifficulty()!=Constants.MEDIUM) {
				otherDiff[i++]=Constants.MEDIUM;
			}
			if(complexity.getDifficulty()!=Constants.DIFFICULT) {
				otherDiff[i]=Constants.DIFFICULT;
			}
			List<Integer> otherIds = new ArrayList<>();
			List<Integer> diffList = new ArrayList<>();;
			for(i=0;i<2;i++) {
				List<Integer> ids = dao.getQuestionIdsForComplexityAndTopics(topicIds, otherDiff[i],null);
				for(int j=0;j<ids.size();j++) {
					diffList.add(otherDiff[i]);
				}
				otherIds.addAll(ids);
			}
			if(!otherIds.isEmpty()) {
				int[] diffArr = new int[diffList.size()];
				i=0;
				for(Integer diff:diffList) {
					diffArr[i++]=diff;
				}
				int reqComplex = requiredComplexity-currentComplexity;
				getRequiredComplexity(reqComplex, diffArr.length, diffArr); 
				// if there is a combination of question which matches the deficient complexity
				// else add questions that can make up to deficient complexity
				if(!indexList.isEmpty()) {
					List<Integer> indexes = indexList.get(new Random().nextInt(indexList.size()));
					for(Integer index:indexes) {
						questionIds.add(otherIds.get(index));
					}
				}else {
					for(int j=0;j<diffList.size();j++) {
						reqComplex-=diffList.get(j);
						if(reqComplex<=0) {
							break;
						}
						questionIds.add(otherIds.get(j));
					}
				}
			}
		}
		return questionIds;
	}
	
	/**
	 * Calculates all possible sum of difficulty level to match up deficient complexity
	 * eg. If required complexity is 30(10*3) ie 10 questions having complexity as "Difficult" but current complexity is 24 ie only 8 questions 
	 * were found for "Difficult" complexity, then there is a deficient of 6(30-24). Now I can find 3 questions having complexity "Medium"
	 * or 6 questions having complexity "Easy" or 2 questions having complexity "Medium" with 2 questions having complexity "Easy". So all 
	 * possible combinations are recored and randomly one choosen.
	 * reference - https://www.geeksforgeeks.org/perfect-sum-problem-print-subsets-given-sum/
	 *
	 * @param sum the deficient complexity
	 * @param n size of question ids array
	 * @param arr array of question ids
	 */
	public void getRequiredComplexity(int sum,int n,int[] arr) {
		if (n == 0 || sum < 0) 
			return; 
		
			// Sum 0 can always be achieved with 0 elements 
			dp = new boolean[n][sum + 1]; 
			for (int i=0; i<n; ++i) 
			{ 
				dp[i][0] = true; 
			} 
		
			// Sum arr[0] can be achieved with single element 
			if (arr[0] <= sum) {
				dp[0][arr[0]] = true;
			}
		
			// Fill rest of the entries in dp[][] 
			for (int i = 1; i < n; ++i) 
				for (int j = 0; j < sum + 1; ++j) {
					dp[i][j] = (arr[i] <= j) ? (dp[i-1][j] || 
											dp[i-1][j-arr[i]]) 
											: dp[i - 1][j]; 
				}
			if (dp[n-1][sum] == false) 
			{ 
				return; 
			} 
		
			// Now recursively traverse dp[][] to find all 
			// paths from dp[n-1][sum] 
			ArrayList<Integer> p = new ArrayList<>(); 
			findCombination(arr, n-1, sum, p); 
	}
	
	/**
	 * Finds if there is any combinations of questions matching up to deficient complexity.
	 *
	 * @param arr array of question ids
	 * @param i the index
	 * @param sum the deficient complexity
	 * @param p the list of all possible question ids
	 */
	public void findCombination(int arr[], int i, int sum, List<Integer> p) {
		// If we reached end and sum is non-zero. We print 
		// p[] only if arr[0] is equal to sun OR dp[0][sum] 
		// is true. 
		if(indexList.size()>5) {
			return;
		}
		if (i == 0 && sum != 0 && dp[0][sum]) 
		{ 
			p.add(i); 
			indexList.add(p); 
			return; 
		} 
	
		// If sum becomes 0 
		if (i == 0 && sum == 0) 
		{ 
			indexList.add(p);  
			return; 
		} 
	
		// If given sum can be achieved after ignoring 
		// current element. 
		if (dp[i-1][sum]) 
		{ 
			// Create a new vector to store path 
			ArrayList<Integer> b = new ArrayList<>(); 
			b.addAll(p); 
			findCombination(arr, i-1, sum, b); 
		} 
		
		// If given sum can be achieved after considering 
		// current element. 
		if (sum >= arr[i] && dp[i-1][sum-arr[i]]) 
		{ 
			p.add(i); 
			findCombination(arr, i-1, sum-arr[i], p); 
		}
	}
}
