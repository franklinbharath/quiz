/**
 * 
 */
package fr.epita.quiz.constants;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants contains constants to avoid hardcoded string or integer comparison.
 * 
 * @author rishi
 */
public class Constants {
	
	/** The Constant OPENQUESTION. */
	public static final int OPENQUESTION = 1;
	
	/** The Constant MCQUESTION. */
	public static final int MCQUESTION = 2;
	
	/** The Constant EASY. */
	public static final int EASY = 1;
	
	/** The Constant MEDIUM. */
	public static final int MEDIUM = 2;
	
	/** The Constant DIFFICULT. */
	public static final int DIFFICULT = 3;
	
	/** The Constant EASYSTR. */
	public static final String EASYSTR = "Easy";
	
	/** The Constant MEDIUMSTR. */
	public static final String MEDIUMSTR = "Medium";
	
	/** The Constant DIFFICULTSTR. */
	public static final String DIFFICULTSTR = "Difficult";
	
	/** The Constant STATUS. */
	public static final String STATUS="STATUS";
	
	/** The Constant SUCCESS. */
	public static final String SUCCESS="SUCCESS";
	
	/** The Constant FAILED. */
	public static final String FAILED="FAILED";
	
	/** The Constant MSG. */
	public static final String MSG="MSG";
}
