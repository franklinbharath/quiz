/**
 * 
 */
package fr.epita.quiz.constants;

// TODO: Auto-generated Javadoc
/**
 * The Class QueryConstants contains constants to avoid integer check, extensively used in ConstructQuery class.
 *
 * @author bharath
 */
public class QueryConstants {
	
	/** The Constant LIKE. */
	public static final int LIKE = 1; 
	
	/** The Constant EQUAL. */
	public static final int EQUAL = 2;
	
	/** The Constant NOTEQUAL. */
	public static final int NOTEQUAL = 3;
	
	/** The Constant IN. */
	public static final int IN = 4;
	
	/** The Constant AND. */
	public static final int AND = 11;
	
	/** The Constant OR. */
	public static final int OR = 12;
	
	/** The Constant INNERJOIN. */
	public static final int INNERJOIN = 21;
	
	/** The Constant LEFTJOIN. */
	public static final int LEFTJOIN = 22;
}
