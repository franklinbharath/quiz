/**
 * 
 */
package fr.epita.quiz.datamodel;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class MCQuestion.
 *
 * @author bharath
 */
public class MCQuestion extends Question {
	
	/** The options. */
	private List<MCQOption> options;

	/**
	 * Gets the options.
	 *
	 * @return the options
	 */
	public List<MCQOption> getOptions() {
		return options;
	}

	/**
	 * Sets the options.
	 *
	 * @param options the new options
	 */
	public void setOptions(List<MCQOption> options) {
		this.options = options;
	}
	
}
