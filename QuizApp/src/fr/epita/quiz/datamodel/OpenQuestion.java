/**
 * 
 */
package fr.epita.quiz.datamodel;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenQuestion.
 *
 * @author bharath
 */
public class OpenQuestion extends Question {

	/** The ans. */
	private Answer ans;

	/**
	 * Gets the answer.
	 *
	 * @return the answer
	 */
	public Answer getAnswer() {
		return ans;
	}
	
	/**
	 * Sets the answer.
	 *
	 * @param ans the new answer
	 */
	public void setAnswer(Answer ans) {
		this.ans = ans;
	}
}
