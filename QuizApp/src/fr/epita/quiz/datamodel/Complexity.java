/**
 * 
 */
package fr.epita.quiz.datamodel;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Complexity.
 *
 * @author bharath
 */
public class Complexity {
	
	/** The select topic. */
	private List<Topic> selectTopic;
	
	/** The difficulty. */
	private int difficulty;
	
	/** The total questions. */
	private int totalQuestions;
	
	/**
	 * Gets the select topic.
	 *
	 * @return the select topic
	 */
	public List<Topic> getSelectTopic() {
		return selectTopic;
	}
	
	/**
	 * Sets the select topic.
	 *
	 * @param selectTopic the new select topic
	 */
	public void setSelectTopic(List<Topic> selectTopic) {
		this.selectTopic = selectTopic;
	}
	
	/**
	 * Gets the difficulty.
	 *
	 * @return the difficulty
	 */
	public int getDifficulty() {
		return difficulty;
	}
	
	/**
	 * Sets the difficulty.
	 *
	 * @param difficulty the new difficulty
	 */
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	
	/**
	 * Gets the total questions.
	 *
	 * @return the total questions
	 */
	public int getTotalQuestions() {
		return totalQuestions;
	}
	
	/**
	 * Sets the total questions.
	 *
	 * @param totalQuestions the new total questions
	 */
	public void setTotalQuestions(int totalQuestions) {
		this.totalQuestions = totalQuestions;
	}
}
