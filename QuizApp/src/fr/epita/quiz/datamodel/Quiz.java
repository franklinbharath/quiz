package fr.epita.quiz.datamodel;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Quiz.
 */
public class Quiz {
	
	/** The title. */
	private String title;
	
	/** The id. */
	private int id;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/** The student. */
	private Student student;
	
	/**
	 * Gets the student.
	 *
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * Sets the student.
	 *
	 * @param student the new student
	 */
	public void setStudent(Student student) {
		this.student = student;
	}
	
	/** The difficulty. */
	private int difficulty;

	/**
	 * Gets the difficulty.
	 *
	 * @return the difficulty
	 */
	public int getDifficulty() {
		return difficulty;
	}

	/**
	 * Sets the difficulty.
	 *
	 * @param difficulty the new difficulty
	 */
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	/** The open questions. */
	private List<OpenQuestion> openQuestions;
	
	/** The mcq questions. */
	private List<MCQuestion> mcqQuestions;

	/**
	 * Gets the open questions.
	 *
	 * @return the open questions
	 */
	public List<OpenQuestion> getOpenQuestions() {
		return openQuestions;
	}

	/**
	 * Sets the open questions.
	 *
	 * @param openQuestions the new open questions
	 */
	public void setOpenQuestions(List<OpenQuestion> openQuestions) {
		this.openQuestions = openQuestions;
	}

	/**
	 * Gets the mcq questions.
	 *
	 * @return the mcq questions
	 */
	public List<MCQuestion> getMcqQuestions() {
		return mcqQuestions;
	}

	/**
	 * Sets the mcq questions.
	 *
	 * @param mcqQuestions the new mcq questions
	 */
	public void setMcqQuestions(List<MCQuestion> mcqQuestions) {
		this.mcqQuestions = mcqQuestions;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}