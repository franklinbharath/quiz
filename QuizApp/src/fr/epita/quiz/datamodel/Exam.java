/**
 * 
 */
package fr.epita.quiz.datamodel;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Exam.
 *
 * @author rishi
 */
public class Exam {
	
	/** The grade. */
	private int grade;
	
	/** The selected options. */
	private List<Integer> wrongOptions;
	
	/** The correct options. */
	private List<Integer> correctOptions;
	
	/** The mcq question. */
	private List<MCQuestion> mcqQuestion;
	
	/** The open question. */
	private List<OpenQuestion> openQuestion;
	
	/** The student. */
	private Student student;
	
	/**
	 * Gets the grade.
	 *
	 * @return the grade
	 */
	public int getGrade() {
		return grade;
	}
	
	/**
	 * Sets the grade.
	 *
	 * @param grade the new grade
	 */
	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	/**
	 * Gets the correct options.
	 *
	 * @return the correct options
	 */
	public List<Integer> getCorrectOptions() {
		return correctOptions;
	}
	
	/**
	 * Sets the correct options.
	 *
	 * @param correctOptions the new correct options
	 */
	public void setCorrectOptions(List<Integer> correctOptions) {
		this.correctOptions = correctOptions;
	}
	
	/**
	 * Gets the mcq question.
	 *
	 * @return the mcq question
	 */
	public List<MCQuestion> getMcqQuestion() {
		return mcqQuestion;
	}
	
	/**
	 * Sets the mcq question.
	 *
	 * @param mcqQuestion the new mcq question
	 */
	public void setMcqQuestion(List<MCQuestion> mcqQuestion) {
		this.mcqQuestion = mcqQuestion;
	}
	
	/**
	 * Gets the open question.
	 *
	 * @return the open question
	 */
	public List<OpenQuestion> getOpenQuestion() {
		return openQuestion;
	}
	
	/**
	 * Sets the open question.
	 *
	 * @param openQuestion the new open question
	 */
	public void setOpenQuestion(List<OpenQuestion> openQuestion) {
		this.openQuestion = openQuestion;
	}

	/**
	 * Gets the wrong options.
	 *
	 * @return the wrong options
	 */
	public List<Integer> getWrongOptions() {
		return wrongOptions;
	}

	/**
	 * Sets the wrong options.
	 *
	 * @param wrongOptions the new wrong options
	 */
	public void setWrongOptions(List<Integer> wrongOptions) {
		this.wrongOptions = wrongOptions;
	}

	/**
	 * Gets the student.
	 *
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * Sets the student.
	 *
	 * @param student the new student
	 */
	public void setStudent(Student student) {
		this.student = student;
	}
}
