/**
 * 
 */
package fr.epita.quiz.datamodel;

import fr.epita.quiz.services.HashPassword;

// TODO: Auto-generated Javadoc
/**
 * The Class Teacher.
 *
 * @author rishi
 */
public class Teacher {
	
	/**
	 * Instantiates a new teacher.
	 *
	 * @param username the username
	 * @param password the password
	 */
	public Teacher(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	/**
	 * Instantiates a new teacher.
	 *
	 * @param username the username
	 * @param password the password
	 * @param confPass the conf pass
	 */
	public Teacher(String username, String password, String confPass) {
		this.username = username;
		this.password = password;
		this.confPass = confPass;
	}

	/** The username. */
	private String username;
	
	/** The password. */
	private String password;
	
	/** The conf pass. */
	private String confPass;
	
	/** The id. */
	private int id;
	
	/**
	 * Checks if is valid pass.
	 *
	 * @return true, if is valid pass
	 */
	public boolean isValidPass() {
		return this.password.equals(this.confPass);
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Hash password.
	 *
	 * @return the string
	 */
	public String hashPassword() {
		return HashPassword.hashPassword(this.password);
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
}
