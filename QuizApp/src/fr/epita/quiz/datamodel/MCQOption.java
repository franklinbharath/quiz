/**
 * 
 */
package fr.epita.quiz.datamodel;


// TODO: Auto-generated Javadoc
/**
 * The Class MCQOption.
 *
 * @author rishi
 */
public class MCQOption {
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MCQOption [option=" + option + ", valid=" + valid + ", id=" + id + ", questionId=" + questionId + "]";
	}
	
	/** The option. */
	private String option;
	
	/** The valid. */
	private boolean valid;
	
	/** The id. */
	private int id;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Checks if is valid.
	 *
	 * @return true, if is valid
	 */
	public boolean isValid() {
		return valid;
	}
	
	/**
	 * Sets the valid.
	 *
	 * @param valid the new valid
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	/** The question id. */
	private int questionId;
	
	/**
	 * Instantiates a new MCQ option.
	 */
	public MCQOption() {
		
	}
	
	/**
	 * Instantiates a new MCQ option.
	 *
	 * @param option the option
	 * @param valid the valid
	 */
	public MCQOption(String option,boolean valid) {
		this.option=option;
		this.valid=valid;
	}
	
	/**
	 * Instantiates a new MCQ option.
	 *
	 * @param option the option
	 * @param valid the valid
	 * @param questionId the question id
	 */
	public MCQOption(String option,boolean valid,int questionId) {
		this.option=option;
		this.valid=valid;
		this.questionId=questionId;
	}
	
	/**
	 * Gets the option.
	 *
	 * @return the option
	 */
	public String getOption() {
		return option;
	}
	
	/**
	 * Sets the option.
	 *
	 * @param option the new option
	 */
	public void setOption(String option) {
		this.option = option;
	}
	
	/**
	 * Gets the question id.
	 *
	 * @return the question id
	 */
	public int getQuestionId() {
		return questionId;
	}
	
	/**
	 * Sets the question id.
	 *
	 * @param questionId the new question id
	 */
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
}
