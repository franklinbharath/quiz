package fr.epita.quiz.datamodel;


import java.util.List;
import java.sql.Blob;

// TODO: Auto-generated Javadoc
/**
 * The Class Question.
 */
public class Question {

	/** The id. */
	private int id;
	
	/** The question. */
	private String question; 
	
	/** The topics. */
	private List<String> topics;	 
	
	/** The difficulty. */
	private Integer difficulty;
	
	/** The type. */
	private Integer type;

	/**
	 * Instantiates a new question.
	 */
	public Question() {
		
	}
	
	/**
	 * Instantiates a new question.
	 *
	 * @param question the question
	 * @param topics the topics
	 * @param difficulty the difficulty
	 * @param type the type
	 */
	public Question(String question, List<String> topics, Integer difficulty, Integer type) {
		this.question = question;
		this.topics = topics;
		this.difficulty = difficulty;
		this.type = type;
	}
	
	/**
	 * Gets the question.
	 *
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	
	/**
	 * Sets the question.
	 *
	 * @param question the new question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	
	/**
	 * Gets the topics.
	 *
	 * @return the topics
	 */
	public List<String> getTopics() {
		return topics;
	}
	
	/**
	 * Sets the topics.
	 *
	 * @param topics the new topics
	 */
	public void setTopics(List<String> topics) {
		this.topics = topics;
	}
	
	/**
	 * Gets the difficulty.
	 *
	 * @return the difficulty
	 */
	public Integer getDifficulty() {
		return difficulty;
	}
	
	/**
	 * Sets the difficulty.
	 *
	 * @param difficulty the new difficulty
	 */
	public void setDifficulty(Integer difficulty) {
		this.difficulty = difficulty;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public Integer getType() {
		return this.type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Question [id=" + id + ", question=" + question + ", topics=" + topics + ", difficulty=" + difficulty
				+ ", type=" + type + "]";
	}
	
	/** The difficult str. */
	private String difficultStr;
	
	/**
	 * Gets the difficult str.
	 *
	 * @return the difficult str
	 */
	public String getDifficultStr() {
		return difficultStr;
	}

	/**
	 * Sets the difficult str.
	 *
	 * @param difficultStr the new difficult str
	 */
	public void setDifficultStr(String difficultStr) {
		this.difficultStr = difficultStr;
	}
}
