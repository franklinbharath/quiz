/**
 * 
 */
package fr.epita.quiz.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class Logger is used to display exceptions in application log for the project.
 *
 * @author rishi
 * 
 */
public class Logger {

	/** The writer. */
	private static PrintWriter writer;
	
	/** The intialized. */
	private static boolean intialized = false;
	
	/**
	 * Log message for error.
	 *
	 * @param msg the error message
	 */
	public static void logMessage(String msg){
		if(!intialized) {
			try {
				writer = new PrintWriter(new FileWriter(new File("application.log"),true));
			}catch(IOException e) {
				e.printStackTrace();
				writer = new PrintWriter(System.out);
			}
		}
		intialized = true;
		String format = "yyyy-MM-dd HH:mm:ss,SSS";
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		writer.println(dateFormat.format(new Date()) +" "+ msg);
		writer.flush();
	}
	
	/**
	 * Log message for exception.
	 *
	 * @param ex the exception
	 */
	public static void logMessage(Exception ex){
		StringWriter outError = new StringWriter();
		ex.printStackTrace(new PrintWriter(outError));
		logMessage(outError.toString());
	}

}
