package fr.epita.quiz.ui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import org.h2.util.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;


import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.datamodel.Answer;
import fr.epita.quiz.datamodel.Complexity;
import fr.epita.quiz.datamodel.Exam;
import fr.epita.quiz.datamodel.MCQOption;
import fr.epita.quiz.datamodel.MCQuestion;
import fr.epita.quiz.datamodel.OpenQuestion;
import fr.epita.quiz.datamodel.Question;
import fr.epita.quiz.datamodel.Quiz;
import fr.epita.quiz.datamodel.Student;
import fr.epita.quiz.datamodel.Teacher;
import fr.epita.quiz.datamodel.Topic;
import fr.epita.quiz.logger.Logger;
import fr.epita.quiz.services.CalulateComplexity;
import fr.epita.quiz.services.InitializeTable;
import fr.epita.quiz.services.QuestionDAO;
import fr.epita.quiz.services.QuestionXMLDAO;
import fr.epita.quiz.services.QuizDAO;
import fr.epita.quiz.services.StudentDAO;
import fr.epita.quiz.services.TeacherDAO;

// TODO: Auto-generated Javadoc
/**
 * The Class UserInterface generates user interface using javaFX
 *
 * @author Bharath
 */

public class UserInterface extends Application {
    
    /** The Constant STAGEHEIGHT. */
    private static final int STAGEHEIGHT = 750;
    
    /** The Constant STAGEWIDTH. */
    private static final int STAGEWIDTH = 900;
    
    /** The Constant LOGIN. */
    private static final String LOGIN = "Login";
    
    /** The Constant SIGNUP. */
    private static final String SIGNUP = "Sign Up";
    
    /** The Constant QUIZ. */
    private static final String QUIZ = "Quiz";
    
    /** The Constant TEACHERCONSOLE. */
    private static final String TEACHERCONSOLE = "Admin Console";
    
    /** The Constant STUDENT. */
    private static final String STUDENT = "Student";
    
    /** The Constant TEACHER. */
    private static final String TEACHER = "Teacher";
    
    /** The user. */
    private String user = STUDENT;
    
    /** The stage. */
    private Stage stage;
    
    /** The student obj. */
    private Student studentObj;
    
    /** The teacher obj. */
    private Teacher teacherObj;

    /* (non-Javadoc)
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
    	InitializeTable tables = new InitializeTable();
    	tables.addTablesToDB();
        // Create UI
    	stage = primaryStage;
        Scene scene = new Scene(showLoginPage(), STAGEWIDTH, STAGEHEIGHT);
        setScene(scene,LOGIN);
    }
    
    /**
     * Sets the scene.
     *
     * @param scene the new scene
     */
    public void setScene(Scene scene) {
    	setScene(scene,null);
    }
    
    /**
     * Sets the scene.
     *
     * @param scene the scene
     * @param title the title
     */
    public void setScene(Scene scene,String title) {
    	if(title!=null) {
    		stage.setTitle(title);
    	}
        stage.setScene(scene); 
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.show(); 
    }
    
    /**
     * Shows login page.
     *
     * @return the grid pane
     */
    public GridPane showLoginPage() {
    	Button btLogin = new Button(LOGIN);
	    Button btNewStudent = new Button("New Student?");
	    Button btNewTeacher = new Button("New Teacher?");
    	user = STUDENT;
    	TextField tfUsername = new TextField();
        PasswordField tfPassword = new PasswordField();
    	GridPane gridPane = new GridPane();
        final ToggleGroup group = new ToggleGroup();
        RadioButton rb1 = new RadioButton(STUDENT);
        rb1.setToggleGroup(group);
        rb1.setSelected(true);
        rb1.setUserData(STUDENT);

        RadioButton rb2 = new RadioButton(TEACHER);
        rb2.setToggleGroup(group);
        rb2.setUserData(TEACHER);
        
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        btNewStudent.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	user = STUDENT;
            	Scene scene = new Scene(constructGridPaneForSignUp(), STAGEWIDTH, STAGEHEIGHT);
            	setScene(scene,SIGNUP); 
            }
        });
        btNewTeacher.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	user = TEACHER;
            	Scene scene = new Scene(constructGridPaneForSignUp(), STAGEWIDTH, STAGEHEIGHT);
            	setScene(scene,SIGNUP);
            }
        });
        btLogin.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override public void handle(ActionEvent e) {
				String username = tfUsername.getText().trim();
				String password = tfPassword.getText().trim();
				if(username.equals("")) {
					showErrorMsg("Enter Username");
				}else if(password.equals("")) {
					showErrorMsg("Enter Password");
			   	}else {
			   		if(STUDENT.equals(user)) {
						 Student stud = new Student(username,password);
						 StudentDAO dao = new StudentDAO();
						 Map<String,Object> resp = dao.validateStudent(stud);
        				 if(resp.containsKey(Constants.STATUS) && resp.get(Constants.STATUS).equals(Constants.FAILED)) {
        					 showErrorMsg(resp.get(Constants.MSG).toString());
        				 }else {
        					 studentObj = (Student)resp.get("Student");
        					 showStudentPage();
        				 }
					 }else {
						 Teacher teach = new Teacher(username,password);
						 TeacherDAO dao = new TeacherDAO();
						 Map<String,Object> resp = dao.validateTeacher(teach);
        				 if(resp.containsKey(Constants.STATUS) && resp.get(Constants.STATUS).equals(Constants.FAILED)) {
        					 showErrorMsg(resp.get(Constants.MSG).toString());
        				 }else {
        					 teacherObj = (Teacher) resp.get("Teacher");
        					 showTeacherpage();
        				 }
					 }
			   	}
            }
        });
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                Toggle oldToggle, Toggle newToggle) {
              if (group.getSelectedToggle() != null) {
            	  tfUsername.clear();
            	  tfPassword.clear();
                if(TEACHER.equals(group.getSelectedToggle().getUserData())){
                	user = TEACHER;
                	gridPane.getChildren().remove(btNewStudent);
                	gridPane.add(btNewTeacher, 0, 4);
                	GridPane.setHalignment(btNewTeacher, HPos.CENTER);
                }else {
                	user = STUDENT;
                	gridPane.getChildren().remove(btNewTeacher);
                	gridPane.add(btNewStudent, 0, 4);
                }
              }
            }
          });
        
        HBox box = new HBox(20, rb1,rb2);
        box.setFillHeight(true);          // Added this
        box.setAlignment(Pos.CENTER_LEFT);// Changed the alignment to center-left
        gridPane.add(box,0,0);
        gridPane.setColumnSpan(box, 2);
 
        gridPane.add(new Label("Username:"), 0, 1);
        gridPane.add(tfUsername, 1, 1);
        gridPane.add(new Label("Password:"), 0, 2);
        gridPane.add(tfPassword, 1, 2);
        gridPane.add(btLogin, 1, 4);
        gridPane.add(btNewStudent, 0, 4);

        // Set properties for UI
        gridPane.setAlignment(Pos.CENTER);
        tfUsername.setAlignment(Pos.BOTTOM_RIGHT);
        tfPassword.setAlignment(Pos.BOTTOM_RIGHT);

        GridPane.setHalignment(btLogin, HPos.CENTER);
        GridPane.setHalignment(btNewStudent, HPos.CENTER);

        gridPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        return gridPane;
    }
    
    /**
     * Constructs grid pane for sign up.
     *
     * @return the grid pane
     */
    public GridPane constructGridPaneForSignUp() {
    	Button btSignUp = new Button(SIGNUP);
    	Button btBack = new Button("Back");
   	 	TextField tfUsername = new TextField();
        PasswordField tfPassword = new PasswordField();
        PasswordField tfCPassword = new PasswordField();
   	 	GridPane gridPane = new GridPane();
   	 	gridPane.add(new Label("Username:"), 0, 1);
        gridPane.add(tfUsername, 1, 1);
        gridPane.add(new Label("Password:"), 0, 2);
        gridPane.add(tfPassword, 1, 2);
        gridPane.add(new Label("Confirm Password:"), 0, 3);
        gridPane.add(tfCPassword, 1, 3);
        gridPane.add(btSignUp, 1, 4);
        gridPane.add(btBack, 0, 4);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        tfUsername.setAlignment(Pos.BOTTOM_RIGHT);
        tfPassword.setAlignment(Pos.BOTTOM_RIGHT);
        tfCPassword.setAlignment(Pos.BOTTOM_RIGHT);
        GridPane.setHalignment(btSignUp, HPos.CENTER);
        GridPane.setHalignment(btBack, HPos.CENTER);
        btSignUp.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
           	 String username = tfUsername.getText().trim();
           	 String password = tfPassword.getText().trim();
           	 String confirmPassword = tfCPassword.getText().trim();
           	 if(username.equals("")) {
           		 showErrorMsg("Enter Username");
           	 }else if(password.equals("")) {
           		 showErrorMsg("Enter Password");
           	 }else if(confirmPassword.equals("")) {
           		 showErrorMsg("Enter Confirm Password");
           	 }else {
           		 if(!confirmPassword.equals(password)) {
           			 showErrorMsg("Password not matching");
           		 }else {
           			 if(STUDENT.equals(user)) {
           				 Student newStudent = new Student(username,password,confirmPassword);
           				 StudentDAO dao = new StudentDAO();
           				 Map<String,Object> resp = dao.createStudent(newStudent);
           				 if(resp.containsKey(Constants.STATUS) && resp.get(Constants.STATUS).equals(Constants.FAILED)) {
           					 showErrorMsg(resp.get(Constants.MSG).toString());
           				 }else {
           					studentObj = (Student)resp.get("Student");
           					showStudentPage();
           				 }
           			 }else {
           				 Teacher newTeacher = new Teacher(username,password,confirmPassword);
           				 TeacherDAO dao = new TeacherDAO();
           				 Map<String,Object> resp = dao.createTeacher(newTeacher);
           				 if(resp.containsKey(Constants.STATUS) && resp.get(Constants.STATUS).equals(Constants.FAILED)) {
           					 showErrorMsg(resp.get(Constants.MSG).toString());
           				 }else {
           					teacherObj = (Teacher) resp.get("Teacher");
           					showTeacherpage();
           				 }
           			 }
           		 }
           	 }
             }
         });
        btBack.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	user = STUDENT;
            	setScene(new Scene(showLoginPage(), STAGEWIDTH, STAGEHEIGHT),LOGIN);
            }
        });
        gridPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
   	 	return gridPane;
    }
    
    /**
     * Shows student page.
     */
    public void showStudentPage() {
    	setScene(new Scene(showStudentView(), STAGEWIDTH, STAGEHEIGHT),QUIZ);
    }
    
    /**
     * Shows student view.
     *
     * @return the grid pane
     */
    public GridPane showStudentView() {
    	GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        VBox vbox = new VBox();
        ObservableList<Node> node = vbox.getChildren();
        Label studentName = new Label("Welcome "+studentObj.getUsername());
        studentName.setStyle("-fx-font-size: 18pt;");
        node.add(studentName);
        node.add(new Label());
        QuestionDAO dao = new QuestionDAO();
        List<Topic> topics = dao.getAllTopics();
        VBox topicVbox = new VBox();
        for(Topic topic:topics) {
        	CheckBox checkBox = new CheckBox(topic.getTopic());
        	checkBox.setId(String.valueOf("topic"+topic.getId()));
        	CustomMenuItem custMenu = new CustomMenuItem(checkBox); 
        	custMenu.setHideOnClick(false);  
        	topicVbox.getChildren().add(checkBox);
        	topicVbox.setPadding(new Insets(10,0,0,10));
        	topicVbox.getChildren().add(new Label());
        }
        ScrollPane s1 = new ScrollPane();
		s1.setPannable(true);
        s1.setPrefSize(140, 100);
        s1.setContent(topicVbox);
        Label topicLabel = new Label("Select Topics");
        topicLabel.setPrefWidth(150);
        node.add(new HBox(5,topicLabel,s1));
        node.add(new Label());
        ComboBox<String> dropDown = new ComboBox<>();
		dropDown.getItems().addAll(
            Constants.EASYSTR,
            Constants.MEDIUMSTR,
            Constants.DIFFICULTSTR 
        );
		dropDown.setId("difficulty");
		dropDown.getSelectionModel().select(Constants.EASYSTR);
		dropDown.setPrefWidth(140);
		Label difficultyLabel = new Label("Difficulty");
		difficultyLabel.setPrefWidth(150);
		node.add(new HBox(5,difficultyLabel,dropDown));
		node.add(new Label());
		
		Label noQues = new Label("No. of Questions");
		noQues.setPrefWidth(150);
		TextField no = new TextField();
		no.setPrefWidth(140);
		no.setId("questionNo");
		node.add(new HBox(5,noQues,no));
		node.add(new Label());
		
        gridPane.add(vbox, 0, 0);
    	Button start = new Button("Start Quiz");
    	start.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	List<Topic> selectTopic = new ArrayList<>();
            	for(Topic topic:topics) {
            		CheckBox optBox = (CheckBox) gridPane.lookup("#topic"+String.valueOf(topic.getId()));
            		if(optBox.isSelected()) {
            			selectTopic.add(topic);
            		}
            	}
            	if(selectTopic.isEmpty() && !showDeleteConfirmation("You have not selected any topic. By default all the topics will be selected. Do you want to continue?")) {
    				return;
    			}
            	String difficulty = dropDown.getSelectionModel().getSelectedItem();
            	int difficultLevel = Constants.EASY;
    			if(Constants.DIFFICULTSTR .equals(difficulty)) {
    				difficultLevel = Constants.DIFFICULT;
    			}else if(Constants.MEDIUMSTR.equals(difficulty)) {
    				difficultLevel = Constants.MEDIUM;
    			}
    			TextField quesNo = (TextField) gridPane.lookup("#questionNo");
    			String numStr = quesNo.getText().trim();
    			int num;
    			if(StringUtils.isNumber(numStr)) {
    				num = Integer.parseInt(numStr);
    				if(num <= 0) {
    					showErrorMsg("Please enter a valid number");
        				return;
    				}
    			}else {
    				showErrorMsg("Please enter a valid number");
    				return;
    			}
            	startQuiz(selectTopic.isEmpty()?topics:selectTopic,difficultLevel,num);
            }
        });
    	Button signOut = new Button("Sign Out");
    	signOut.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
    		@Override public void handle(ActionEvent e) {
    			Scene scene = new Scene(showLoginPage(), STAGEWIDTH, STAGEHEIGHT);
    	        setScene(scene,LOGIN);
    		}
    	});
    	gridPane.add(start, 0, 6);
    	gridPane.add(new Label(), 0, 7);
    	gridPane.add(signOut, 0, 8);
    	gridPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
    	return gridPane;
    }
    
    /**
     * Starts quiz for given topics, difficulty and number of questions.
     *
     * @param selectTopic the selected topic
     * @param selectDiff the selected difficulty
     * @param selectNum the selected number of questions
     */
    public void startQuiz(List<Topic> selectTopic,int selectDiff,int selectNum) {
    	GridPane gridPane = new GridPane();
    	gridPane.setAlignment(Pos.TOP_LEFT);
    	gridPane.setHgap(5);
        gridPane.setVgap(5);
        Complexity complexity = new Complexity();
        complexity.setDifficulty(selectDiff);
        complexity.setSelectTopic(selectTopic);
        complexity.setTotalQuestions(selectNum);
        CalulateComplexity calComplexity = new CalulateComplexity();
        calComplexity.setComplexity(complexity);
        List<Integer> ids = calComplexity.getQuestionsForComplexity();
        if(ids.isEmpty()) {
        	showErrorMsg("No Questions were found for given topics and complexity");
        	return;
        }
        QuizDAO quizDAO = new QuizDAO();
        Quiz quiz = quizDAO.createQuiz(ids,studentObj);
    	Map<Integer,MCQuestion> mcqMap = new HashMap<>();
    	Map<Integer,OpenQuestion> openMap = new HashMap<>();
    	List<Question> questions = new ArrayList<>();
    	List<MCQuestion> mcQuestion = quiz.getMcqQuestions();
		List<OpenQuestion> openQuestion = quiz.getOpenQuestions();
		Collections.shuffle(mcQuestion);
		Collections.shuffle(openQuestion);
		VBox vbox = new VBox();
		ObservableList<Node> node = vbox.getChildren();
		Button exportTxt = new Button();
		exportTxt.setText("Export as Text File");
		Button exportPDF = new Button();
		exportPDF.setText("Export as PDF");
		HBox titleBox = new HBox(20,new Label(quiz.getTitle()),exportTxt,exportPDF);
		node.add(titleBox);
		node.add(new Label());
		int i=1;
		for(MCQuestion mcq:mcQuestion) {
			questions.add(mcq);
			mcqMap.put(mcq.getId(), mcq);
		}
		for(OpenQuestion open:openQuestion) {
			questions.add(open);
			openMap.put(open.getId(), open);
		}
		Collections.shuffle(questions);
		for(Question question:questions) {
			int questionId = question.getId();
			if(question.getType() == Constants.MCQUESTION) {
				MCQuestion mcq = mcqMap.get(questionId);
				TextArea textArea = new TextArea(mcq.getQuestion());
				textArea.setDisable(true);
				Label label = new Label(String.valueOf(i++)+"."+mcq.getQuestion());
				label.setMinHeight(Region.USE_PREF_SIZE);
				node.add(label);
				node.add(new Label());
				List<MCQOption> options = mcq.getOptions();
				Collections.shuffle(options);
				VBox checkList = new VBox();
				ObservableList<Node> checkListNode = checkList.getChildren();
				for(MCQOption option:options) {
					CheckBox checkBox = new CheckBox();
					checkBox.setMinWidth(500);
					checkBox.setText(option.getOption());
					checkBox.setId(String.valueOf("option-"+option.getId()));
					checkListNode.add(checkBox);
					checkListNode.add(new Label());
				}
				checkList.setId(String.valueOf(mcq.getId()));
				node.add(checkList);
				node.add(new Label());
			}else {
				OpenQuestion open = openMap.get(questionId);
				Label label = new Label(String.valueOf(i++)+"."+open.getQuestion());
				label.setMinHeight(Region.USE_PREF_SIZE);
				node.add(label);
				node.add(new Label());
				TextArea ansArea = new TextArea();
				ansArea.setMinWidth(350);
				ansArea.setMaxHeight(100);
				ansArea.setMinHeight(100);
				ansArea.setMaxWidth(350);
				ansArea.setId(String.valueOf(open.getId()));
				node.add(ansArea);
				node.add(new Label());
			}
			
		}
		exportPDF.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	exportAsPDF(mcqMap, openMap, questions);
            }
		});
		exportTxt.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	exportAsTxt(mcqMap, openMap, questions);
            }
		});
		gridPane.setHgap(5);
        gridPane.setVgap(5);
		gridPane.add(vbox, 4, 4);
		ScrollPane s1 = new ScrollPane();
		s1.setPannable(true);
        s1.setPrefSize(STAGEWIDTH, STAGEHEIGHT);
        s1.setContent(gridPane);
		Button finish = new Button("Finish");
		finish.setId("finishBt");
		finish.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	for(MCQuestion mcq:mcQuestion) {
            		VBox vbox = (VBox) gridPane.lookup("#"+String.valueOf(mcq.getId()));
            		List<MCQOption> options = mcq.getOptions();
            		for(MCQOption option:options) {
            			CheckBox checkBox= (CheckBox) vbox.lookup("#option-"+String.valueOf(option.getId()));
            			option.setValid(checkBox.isSelected());
            		}
            	}
            	for(OpenQuestion open:openQuestion) {
        			TextArea ansArea = (TextArea) gridPane.lookup("#"+String.valueOf(open.getId()));
        			Answer ans = open.getAnswer();
        			ans.setAnswer(ansArea.getText());
        		}
            	Exam exam = quizDAO.evaluateQuiz(quiz);
            	List<Integer> wrongIds = exam.getWrongOptions();
				List<Integer> correctIds = exam.getCorrectOptions();
            	for(Integer ids:wrongIds) {
            		CheckBox checkBox= (CheckBox) vbox.lookup("#option-"+String.valueOf(ids));
            		checkBox.setStyle("-fx-background-color:red;-fx-text-fill: white");
            	}
            	for(Integer ids:correctIds) {
            		CheckBox checkBox= (CheckBox) vbox.lookup("#option-"+String.valueOf(ids));
            		checkBox.setStyle("-fx-background-color:green;-fx-text-fill: white");
            	}
            	Label result = new Label();
            	result.setMinHeight(Region.USE_PREF_SIZE);
            	String resultStr = null;
            	if(!mcQuestion.isEmpty()) {
            		resultStr = "Your score is "+exam.getGrade()+"/"+String.valueOf(mcQuestion.size()+".")+"(Mutiple Choice Question Only).";
            	}
            	if(!openQuestion.isEmpty()) {
            		if(resultStr!=null) {
            			resultStr += "\nYour answers to open questions are being processed.";
            		}else {
            			resultStr = "\nYour answers open questions being processed.";
            		}
            	}
            	result.setText(resultStr);
            	result.setPadding(new Insets(25, 25, 25, 30));
            	result.setAlignment(Pos.CENTER);

            	result.setStyle("-fx-background-color: rgb(" + 245 + "," + 245 + ", " + 245 + ");");
            	Button back = new Button("Go to home");
            	back.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                    	showStudentPage();
                    }
            	});
            	Button exportRes = new Button("Export result");
            	exportRes.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                    	exportResultAsPDF(exam, mcqMap, openMap, questions);
                    }
            	});
            	s1.setVvalue(1.0);
            	VBox box = new VBox(10,result,new HBox(10,back,exportRes));
            	gridPane.add(box, 4, 5);
            	
            }
        });
		gridPane.add(finish, 4, 5);
		gridPane.add(new Label(), 4, 6);
		gridPane.setPrefSize(STAGEWIDTH-20, STAGEHEIGHT);
		gridPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		setScene(new Scene(s1, STAGEWIDTH, STAGEHEIGHT));
    }
    
    /**
     * Export result as PDF.
     *
     * @param exam the exam
     */
    private void exportResultAsPDF(Exam exam,Map<Integer, MCQuestion> mcqMap, Map<Integer, OpenQuestion> openMap,
			List<Question> questions) {
    	try {
			FileChooser fileChooser = new FileChooser();
            //Set extension filter
            FileChooser.ExtensionFilter extFilter = 
                new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
            fileChooser.getExtensionFilters().add(extFilter);
            //Show save file dialog
            File file = fileChooser.showSaveDialog(stage);
            if(file!=null) {
            	 constructPDFContent(exam,mcqMap, openMap, questions,file);
            }
        } catch (Exception ex) {
            Logger.logMessage(ex);
        }
    }
    
    /**
     * Construct PDF content for exam result.
     *
     * @param exam the exam
     * @param mcqMap the mcq map
     * @param openMap the open map
     * @param questions the questions
     * @param file the file
     */
    private void constructPDFContent(Exam exam, Map<Integer, MCQuestion> mcqMap, Map<Integer, OpenQuestion> openMap,
			List<Question> questions, File file) {
    	try(OutputStream ofile = new FileOutputStream(file);){
    		Document document = new Document();
		    PdfWriter.getInstance(document, ofile);
		    document.open();
		    List<Integer> correctOpts = exam.getCorrectOptions();
		    List<Integer> wrongOpts = exam.getWrongOptions();
		    int i=1;
		    String resultStr = null;
        	if(!mcqMap.isEmpty()) {
        		resultStr = "Your score is "+exam.getGrade()+"/"+String.valueOf(mcqMap.size()+"(Mutiple Choice Question Only).");
        	}
        	if(!openMap.isEmpty()) {
        		if(resultStr!=null) {
        			resultStr += "\nYour answers to open questions are being processed.";
        		}else {
        			resultStr = "\nYour answers open questions being processed.";
        		}
        	}
        	Chunk resultChunk= new Chunk(resultStr);
        	Paragraph para = new Paragraph();
        	para.add(resultChunk);
        	para.setAlignment(Element.ALIGN_CENTER);
        	document.add(para);
        	
			for(Question question:questions) {
				int questionId = question.getId();
				if(question.getType() == Constants.MCQUESTION) {
					MCQuestion mcq = mcqMap.get(questionId);
					document.add(new Paragraph((i++)+"."+mcq.getQuestion()));
					document.add(new Paragraph("\n"));
					List<MCQOption> options = mcq.getOptions();
					Collections.shuffle(options);
					char c = 97;
					for(MCQOption option:options) {
						if(correctOpts.contains(option.getId())) {
							Chunk chunk = new Chunk("\t"+(c++)+"."+option.getOption());
							chunk.setBackground(BaseColor.GREEN);
							Paragraph p = new Paragraph(chunk);
							document.add(p);
						}else if(wrongOpts.contains(option.getId())) {
							Chunk chunk = new Chunk("\t"+(c++)+"."+option.getOption());
							chunk.setBackground(BaseColor.RED);
							Paragraph p = new Paragraph(chunk);
							document.add(p);
						}else {
							document.add(new Paragraph("\t"+(c++)+"."+option.getOption()));
						}
						document.add(new Paragraph("\n"));
					}
					document.add(new Paragraph("\n"));
				}else {
					OpenQuestion open = openMap.get(questionId);
					document.add(new Paragraph((i++)+"."+open.getQuestion()));
					document.add(new Paragraph(open.getAnswer().getAnswer()));
					document.add(new Paragraph("\n"));
				}
			}
			document.close();
    	}catch(Exception ex) {
			Logger.logMessage(ex);
		}
    }
    /**
     * Export as generated Quiz as PDF.
     *
     * @param mcqMap the mcq questions
     * @param openMap the open questions
     * @param questions the questions
     */
    private void exportAsPDF(Map<Integer, MCQuestion> mcqMap, Map<Integer, OpenQuestion> openMap,
			List<Question> questions) {
    	try {
			FileChooser fileChooser = new FileChooser();
            //Set extension filter
            FileChooser.ExtensionFilter extFilter = 
                new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
            fileChooser.getExtensionFilters().add(extFilter);
            //Show save file dialog
            File file = fileChooser.showSaveDialog(stage);
            if(file!=null) {
	            constructPDFContent(mcqMap, openMap, questions, file);
            }
        } catch (Exception ex) {
            Logger.logMessage(ex);
        }
	}

	/**
	 * Construct PDF content.
	 *
	 * @param mcqMap the mcq questions
	 * @param openMap the open questions
	 * @param questions the questions
	 * @param file the file
	 */
	private void constructPDFContent(Map<Integer, MCQuestion> mcqMap, Map<Integer, OpenQuestion> openMap,
			List<Question> questions, File file) {
		try(OutputStream ofile = new FileOutputStream(file);){
		    Document document = new Document();
		    PdfWriter.getInstance(document, ofile);
		    document.open();
		    int i=1;
			for(Question question:questions) {
				int questionId = question.getId();
				if(question.getType() == Constants.MCQUESTION) {
					MCQuestion mcq = mcqMap.get(questionId);
					document.add(new Paragraph((i++)+"."+mcq.getQuestion()));
					document.add(new Paragraph("\n"));
					List<MCQOption> options = mcq.getOptions();
					Collections.shuffle(options);
					char c = 97;
					for(MCQOption option:options) {
						document.add(new Paragraph("\t"+(c++)+"."+option.getOption()));
						document.add(new Paragraph("\n"));
					}
					document.add(new Paragraph("\n"));
				}else {
					OpenQuestion open = openMap.get(questionId);
					document.add(new Paragraph((i++)+"."+open.getQuestion()));
					for(int j=0;j<5;j++) {
						document.add(new Paragraph("\n"));
					}
				}
			}
		    document.close();
		}catch(Exception ex) {
			Logger.logMessage(ex);
		}
	}
    
    /**
     * Export as generated Quiz as txt.
     *
     * @param mcqMap the mcq questions
     * @param openMap the open questions
     * @param questions the questions
     */
    private void exportAsTxt(Map<Integer, MCQuestion> mcqMap, Map<Integer, OpenQuestion> openMap,List<Question> questions) {
		FileChooser fileChooser = new FileChooser();
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = 
            new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        //Show save file dialog
        File file = fileChooser.showSaveDialog(stage);
        if(file != null){
        	try {
                constructTextContent(mcqMap, openMap, questions, file);
            } catch (Exception ex) {
            	Logger.logMessage(ex);
            }
        }
	}
    
	/**
	 * Construct text content.
	 *
	 * @param mcqMap the mcq questions
	 * @param openMap the open questions
	 * @param questions the questions
	 * @param file the file
	 */
	private void constructTextContent(Map<Integer, MCQuestion> mcqMap, Map<Integer, OpenQuestion> openMap,
			List<Question> questions, File file) {
		try(FileWriter fileWriter = new FileWriter(file);){
		    int i=1;
			for(Question question:questions) {
				int questionId = question.getId();
				if(question.getType() == Constants.MCQUESTION) {
					MCQuestion mcq = mcqMap.get(questionId);
					fileWriter.write((i++)+"."+mcq.getQuestion());
					fileWriter.write("\n");
					List<MCQOption> options = mcq.getOptions();
					Collections.shuffle(options);
					char c = 97;
					for(MCQOption option:options) {
						fileWriter.write("\t"+(c++)+"."+option.getOption());
						fileWriter.write("\n");
					}
					fileWriter.write("\n");
				}else {
					OpenQuestion open = openMap.get(questionId);
					fileWriter.write((i++)+"."+open.getQuestion());
					for(int j=0;j<5;j++) {
						fileWriter.write("\n");
					}
				}
			}
		}catch(Exception ex) {
			Logger.logMessage(ex);
		}
	}
    
    /**
     * Show teacher page.
     */
    public void showTeacherpage() {
    	Scene scene = new Scene(showTeacherView(), STAGEWIDTH, STAGEHEIGHT);
    	setScene(scene,TEACHERCONSOLE);
    }
    
    /**
     * Show teacher view.
     *
     * @return the grid pane
     */
    public GridPane showTeacherView() {
    	return showTeacherView(null);
    }
    
    /**
     * Show teacher view.
     *
     * @param searchQues the search question
     * @return the grid pane
     */
    public GridPane showTeacherView(Question searchQues) {
    	GridPane gridPane = new GridPane();
    	gridPane.setAlignment(Pos.TOP_LEFT);
    	gridPane.setHgap(5);
        gridPane.setVgap(5);
        TableView<Question> table = new TableView<Question>();
        table.setEditable(true);
 
        TableColumn<Question,String> question = new TableColumn<>("Questions");
        TableColumn<Question,String> topic = new TableColumn<>("Topic");
        TableColumn<Question,String> diff = new TableColumn<>("Difficulty");
        
        
        table.getColumns().addAll(question, topic, diff);
        question.setMaxWidth(480);
        question.setMinWidth(480);
        topic.setMaxWidth(150);
        topic.setMinWidth(150);
        table.setMinWidth(860);
        table.setMinHeight(650);
        
        QuestionDAO dao = new QuestionDAO();
        List<Question> questions = dao.searchQuestions(searchQues!=null?searchQues:new Question(null,null,null,null));
        ObservableList<Question> obsQuestions = FXCollections.observableArrayList(questions);
        question.setCellValueFactory(
        	    new PropertyValueFactory<Question,String>("question")
        	);
        topic.setCellValueFactory(
                new PropertyValueFactory<Question, String>("topics"));
        diff.setCellValueFactory(
                new PropertyValueFactory<Question, String>("difficultStr"));

        table.setItems(obsQuestions);
        TableColumn<Question, Integer> actionCol = addButtonToTable();
        actionCol.setCellValueFactory(new PropertyValueFactory<Question, Integer>("id"));
        table.getColumns().add(actionCol);
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10,0, 0, 0));
        Button addQuestion = new Button("Add Question");
    	addQuestion.setOnAction((ActionEvent event) -> {
    		showAddOrUpdateForm(null);
        });
    	Button importXMl = new Button("Import from XML");
    	importXMl.setOnAction((ActionEvent event) -> {
    		FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("XML Files", "*.xml");
    		FileChooser fc = new FileChooser();
    		fc.getExtensionFilters().add(imageFilter);
    		File selectedFile= fc.showOpenDialog(null);
    		if (selectedFile != null) {
    			QuestionXMLDAO xml = new QuestionXMLDAO();
    			xml.saveQuestionsFromXML(selectedFile);
    			showTeacherpage();
    		}
        });
    	Button searchQuestion = new Button("Search");
    	TextField ques = new TextField();
    	if(searchQues!=null && searchQues.getQuestion()!=null && !"".equals(searchQues.getQuestion())) {
    		ques.setText(searchQues.getQuestion());
    	}
    	ques.setId("ques");
    	ques.setPromptText("Question");
    	ques.setPrefWidth(470);
    	TextField top = new TextField();
    	if(searchQues!=null && searchQues.getTopics()!=null) {
    		top.setText(String.join(",", searchQues.getTopics()));
    	}
    	top.setId("top");
    	top.setPromptText("Topic");
    	top.setPrefWidth(120);
    	ComboBox<String> dropDown = new ComboBox<>();
		dropDown.getItems().addAll(
            Constants.EASYSTR,
            Constants.MEDIUMSTR,
            Constants.DIFFICULTSTR 
        );
		//dropDown.getSelectionModel().select(Constants.EASYSTR);
		if(searchQues!=null && searchQues.getDifficulty()!=null) {
			if(searchQues.getDifficulty() == Constants.DIFFICULT) {
				dropDown.getSelectionModel().select(Constants.DIFFICULTSTR);
			}else if(searchQues.getDifficulty() == Constants.MEDIUM) {
				dropDown.getSelectionModel().select(Constants.MEDIUMSTR);
			}else if(searchQues.getDifficulty() == Constants.EASY) {
				dropDown.getSelectionModel().select(Constants.EASYSTR);
			}
		}
		dropDown.setPrefWidth(80);
    	searchQuestion.setOnAction(search(ques, top, dropDown));
    	Button clearSearch = new Button("Clear");
    	clearSearch.setOnAction((ActionEvent event) -> {
    		setScene(new Scene(showTeacherView(), STAGEWIDTH, STAGEHEIGHT),TEACHERCONSOLE);
        });
    	HBox box = new HBox(20,ques,top,dropDown,searchQuestion,clearSearch);
       
        Button signOut = new Button("Sign Out");
        vbox.getChildren().addAll(box,table);
        signOut.setOnAction((ActionEvent event) -> {
        	Scene scene = new Scene(showLoginPage(), STAGEWIDTH, STAGEHEIGHT);
	        setScene(scene,LOGIN);
        });
        Label teachName = new Label(teacherObj.getUsername()+",");
        teachName.setStyle("-fx-font-size: 12pt;");
        HBox addImp = new HBox(5,addQuestion,importXMl); 
        addImp.setAlignment(Pos.TOP_LEFT);
        HBox teacherName = new HBox(5,teachName,signOut);
        teacherName.setAlignment(Pos.TOP_RIGHT);
        gridPane.add(new HBox(505,addImp,teacherName),4,3);
        gridPane.add(vbox,4,4);
 
    	gridPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
    	return gridPane;
    }
	
	/**
	 * Search.
	 *
	 * @param ques the question
	 * @param top the top
	 * @param dropDown the drop down
	 * @return the event handler
	 */
	private EventHandler<ActionEvent> search(TextField ques, TextField top, ComboBox<String> dropDown) {
		return (ActionEvent event) -> {
    		Arrays.asList(top.getText().split(","));
    		Question search = new Question();
    		search.setQuestion(ques.getText());
    		if(!Arrays.asList(top.getText().split(",")).isEmpty()) {
    			search.setTopics(Arrays.asList(top.getText().split(",")));
    		}else {
    			search.setTopics(null);
    		}
    		Object diffObj = dropDown.getSelectionModel().getSelectedItem();
    		if(diffObj!=null) {
	    		String difficulty = dropDown.getSelectionModel().getSelectedItem();
	    		if(!"".equals(difficulty)) {
	    			int difficultLevel;
	    			if(Constants.DIFFICULTSTR .equals(difficulty)) {
	    				difficultLevel = Constants.DIFFICULT;
	    			}else if(Constants.MEDIUMSTR.equals(difficulty)) {
	    				difficultLevel = Constants.MEDIUM;
	    			}else {
	    				difficultLevel = Constants.EASY;
	    			}
	    			search.setDifficulty(difficultLevel);
	    		}
    		}else {
    			search.setDifficultStr(null);
    		}
    		Scene scene = new Scene(showTeacherView(search), STAGEWIDTH, STAGEHEIGHT);
        	setScene(scene,TEACHERCONSOLE);
        };
	}
    
    /**
     * Adds the button to table.
     *
     * @return the table column
     */
    private TableColumn<Question, Integer> addButtonToTable() {
        TableColumn<Question, Integer> colBtn = new TableColumn<>("Action");

        Callback<TableColumn<Question, Integer>, TableCell<Question, Integer>> cellFactory = new Callback<TableColumn<Question, Integer>, TableCell<Question, Integer>>() {
            @Override
            public TableCell<Question, Integer> call(final TableColumn<Question, Integer> param) {
                final TableCell<Question, Integer> cell = new TableCell<Question, Integer>() {
                		Button editButton = new Button("Edit");
                		Button deleteButton = new Button("Delete");
                		HBox pane = new HBox(20,editButton, deleteButton);
	                    {
	                    	editButton.setOnAction((ActionEvent event) -> {
	                    		ObservableList<Question> list = getTableView().getItems();
	                    		Question data = list.get(getIndex());
	                    		showAddOrUpdateForm(data);
	                        });
	                    }
	
	                    {
	                    	deleteButton.setOnAction((ActionEvent event) -> {
	                    		if(showDeleteConfirmation("Are you sure want to delete the question?")) {
		                    		ObservableList<Question> list = getTableView().getItems();
		                        	Question data = list.get(getIndex());
		                        	QuestionDAO dao = new QuestionDAO();
		                        	dao.deleteQuestion(data.getId());
		                        	list.remove(data);
	                    		}
	                        });
	                    }

	                    @Override
	                    public void updateItem(Integer item, boolean empty) {
	                        super.updateItem(item, empty);
	                        if (empty) {
	                            setGraphic(null);
	                        } else {
	                            setGraphic(pane);
	                        }
	                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);
        return colBtn;

    }
    
    /**
     * Show add or update form.
     *
     * @param data the question
     */
    private void showAddOrUpdateForm(Question data) {
		QuestionDAO dao = new QuestionDAO();
		
		GridPane gridPane = new GridPane();
		final Stage dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		VBox dialogVbox = new VBox(20);
		ObservableList<Node> node= dialogVbox.getChildren();
		TextArea questionArea = new TextArea(data!=null?data.getQuestion():"");
		questionArea.setMinWidth(350);
		questionArea.setMaxHeight(70);
		questionArea.setMinHeight(70);
		questionArea.setMaxWidth(350);
		questionArea.setId("question");
		questionArea.getStyleClass().add("check");
		HBox box = new HBox(10, new Label("Question"),questionArea);
		node.add(box);
		TextArea topicsArea = new TextArea(data!=null?String.join(",", data.getTopics()):"");
		topicsArea.setMinWidth(350);
		topicsArea.setMaxHeight(70);
		topicsArea.setMinHeight(70);
		topicsArea.setMaxWidth(350);
		topicsArea.setId("topics");
		box = new HBox(10, new Label("Topics    "),topicsArea);
		node.add(box);
		ComboBox<String> dropDown = new ComboBox<>();
		dropDown.getItems().addAll(
            Constants.EASYSTR,
            Constants.MEDIUMSTR,
            Constants.DIFFICULTSTR 
        );
		dropDown.getSelectionModel().select(data!=null?data.getDifficultStr():Constants.EASYSTR);
		dropDown.setId("difficulty");
		dropDown.getSelectionModel().getSelectedItem();
        node.add(new HBox(10, new Label("Difficulty"),dropDown));
		final ToggleGroup group = new ToggleGroup();
		RadioButton rb1 = new RadioButton("Open Question");
		rb1.setToggleGroup(group);
		if(data == null || data.getType() == Constants.OPENQUESTION) {
			rb1.setSelected(true);
		}
		rb1.setUserData("open");
		rb1.setId("openRadio");
		RadioButton rb2 = new RadioButton("MCQ");
		rb2.setToggleGroup(group);
		if(data!=null && data.getType() == Constants.MCQUESTION) {
			rb2.setSelected(true);
		}
		rb2.setUserData("mcq");
		rb2.setId("mcqRadio");
		box = new HBox(20, rb1,rb2);
		box.setFillHeight(true);          // Added this
		box.setAlignment(Pos.CENTER_LEFT);// Changed the alignment to center-left
	
		node.add(new HBox(20,new Label("Type    "),box));
		
		if(data!=null && data.getType() == Constants.MCQUESTION) {
			VBox optBox = new VBox(20);
			ObservableList<Node> optNode= optBox.getChildren();
			List<MCQOption> mcqOptionList = dao.getOptionsForMCQuestion(data.getId());
			for(MCQOption option:mcqOptionList) {
				HBox optHBox = creationOptionForQuestion(optNode, option);
				optNode.add(optHBox);
			}
			optBox.setId("optBox");
			node.add(new HBox(20, new Label("Options"),optBox));
		}else {
			TextArea ansArea = new TextArea();
			if(data!=null) {
				Answer ans = dao.getAnwerForQuestion(data.getId());
				ansArea.setText(ans.getAnswer());
				ansArea.setUserData(ans.getId());
			}else {
				ansArea.setText("");
				ansArea.setUserData(-1);
			}
			ansArea.setMinWidth(350);
			ansArea.setMaxHeight(100);
			ansArea.setMinHeight(100);
			ansArea.setMaxWidth(350);
			ansArea.setId("answer");
			box = new HBox(10, new Label("Answer    "),ansArea);
			node.add(box);
		}
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                Toggle oldToggle, Toggle newToggle) {
              if (group.getSelectedToggle() != null) {
            	node.remove(4);
                if("open".equals(group.getSelectedToggle().getUserData())){
                	TextArea ansArea = new TextArea();
                	if(data!=null && data.getType() == Constants.OPENQUESTION) {
                		Answer ans = dao.getAnwerForQuestion(data.getId());
                		ansArea.setText(ans.getAnswer());
            			ansArea.setUserData(ans.getId());
                	}else {
                		ansArea.setText("");
                		ansArea.setUserData(-1);
                	}
                	ansArea.setMinWidth(350);
                	ansArea.setId("answer");
        			ansArea.setMaxHeight(100);
        			ansArea.setMinHeight(100);
        			ansArea.setMaxWidth(350);
        			node.add(new HBox(10, new Label("Answer    "),ansArea));
                }else {
                	VBox optBox = new VBox(20);
        			ObservableList<Node> optNode= optBox.getChildren();
                	if(data!=null && data.getType() == Constants.MCQUESTION) {
            			List<MCQOption> mcqOptionList = dao.getOptionsForMCQuestion(data.getId());
            			for(MCQOption option:mcqOptionList) {
            				HBox optHBox = creationOptionForQuestion(optNode, option);
            				optNode.add(optHBox);
            			}
            		}else {
            			MCQOption newOption = new MCQOption();
        				newOption.setOption("");
        				newOption.setValid(false);
        				newOption.setId(-1);
        				optNode.add(creationOptionForQuestion(optNode, newOption));
            			optNode.add(creationOptionForQuestion(optNode, newOption));
            		}
                	optBox.setId("optBox");
                	node.add(new HBox(20, new Label("Options"),optBox));
                }
              }
            }
          });
		gridPane.add(dialogVbox, 4, 4);
		Button saveButton = new Button("Save");
		Button cancelButton = new Button("Cancel");
		gridPane.add(new HBox(),4,5);
		gridPane.add(new HBox(20,saveButton,cancelButton),4,6);
		gridPane.add(new HBox(),4,7);
		
		gridPane.setAlignment(Pos.TOP_LEFT);
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		
        ScrollPane s1 = new ScrollPane();
        s1.setPannable(true);
        s1.setPrefSize(600, 500);
        s1.setContent(gridPane);
        Group root = new Group();
        root.getChildren().add(s1);
		Scene dialogScene = new Scene(root, 600, 500);
		dialog.setScene(dialogScene);
		if(data!=null) {
			dialog.setTitle("Update Question");
		}else {
			dialog.setTitle("Add Question");
		}
		dialog.show();
		saveButton.setOnAction((ActionEvent event) -> {
			TextArea question = (TextArea) dialogScene.lookup("#question");
			String questionStr = question.getText().trim();
			if("".equals(questionStr)) {
				showErrorMsg("Enter a valid question");
				return;
			}
			TextArea topics = (TextArea) dialogScene.lookup("#topics");
			String topicStr = topics.getText().trim();
			if("".equals(topicStr)) {
				showErrorMsg("Please enter a topic");
				return;
			}
			List<String> topicList = new ArrayList<String>(Arrays.asList(topicStr.split(",")));
			ComboBox difficulty = (ComboBox) dialogScene.lookup("#difficulty");
			String diff = difficulty.getSelectionModel().getSelectedItem().toString();
			int difficultLevel;
			if("Difficult".equals(diff)) {
				difficultLevel = Constants.DIFFICULT;
			}else if("Medium".equals(diff)) {
				difficultLevel = Constants.MEDIUM;
			}else {
				difficultLevel = Constants.EASY;
			}
			RadioButton openRadio = (RadioButton) dialogScene.lookup("#openRadio");
			if(openRadio.isSelected()) {
				TextArea answer = (TextArea) dialogScene.lookup("#answer");
				String answerStr = answer.getText().trim();
				if("".equals(answerStr)) {
					showErrorMsg("Please enter a valid answer");
					return;
				}
				Answer ans = new Answer();
				ans.setAnswer(answerStr);
				ans.setId(Integer.parseInt(answer.getUserData().toString()));
				OpenQuestion openQues = new OpenQuestion();
				openQues.setQuestion(questionStr);
				openQues.setTopics(topicList);
				openQues.setAnswer(ans);
				openQues.setDifficulty(difficultLevel);
				openQues.setType(Constants.OPENQUESTION);
				QuestionDAO dataObj = new QuestionDAO();
				if(data!=null) {
					ans.setQuestionId(data.getId());
					openQues.setId(data.getId());
					dataObj.updateOpenQuestion(openQues);
				}else {
					dataObj.createOpenQuestion(openQues);
				}
				dialog.close();
				showTeacherpage();
			}else {
				List<MCQOption> mcqOptList = new ArrayList<>();
				VBox voptBox = (VBox) dialogScene.lookup("#optBox");
				int optBoxSize = voptBox.getChildren().size();
				int checkCount=0;
				for(int i=0;i<optBoxSize;i++) {
					HBox hoptBox = (HBox) voptBox.getChildren().get(i);
					int innerSize = hoptBox.getChildren().size();
					for(int j=0;j<innerSize;j+=2) {
						MCQOption opt = new MCQOption();
						HBox hoptBox1 = (HBox) hoptBox.getChildren().get(j);
						CheckBox  checkBox= (CheckBox) hoptBox1.getChildren().get(0);
						TextField  textField= (TextField) hoptBox1.getChildren().get(1);
						String optionStr = textField.getText().trim();
						if("".equals(optionStr)) {
							showErrorMsg("Please enter option");
							return;
						}
						opt.setId(Integer.parseInt(checkBox.getUserData().toString()));
						opt.setOption(optionStr);
						if(data!=null) {
							opt.setQuestionId(data.getId());
						}
						if(checkBox.isSelected()) {
							checkCount++;
						}
						opt.setValid(checkBox.isSelected());
						mcqOptList.add(opt);
					}
				}
				if(checkCount == 0) {
					showErrorMsg("Atleast one option should be selected as an answer");
					return;
				}
				MCQuestion mcqQuestion = new MCQuestion();
				mcqQuestion.setDifficulty(difficultLevel);
				mcqQuestion.setOptions(mcqOptList);
				mcqQuestion.setQuestion(questionStr);
				mcqQuestion.setTopics(topicList);
				mcqQuestion.setType(Constants.MCQUESTION);
				QuestionDAO dataObj = new QuestionDAO();
				if(data!=null) {
					mcqQuestion.setId(data.getId());
					dataObj.updateMCQuestion(mcqQuestion);
				}else {
					dataObj.createMCQuestion(mcqQuestion);
				}
				dialog.close();
				showTeacherpage();
			}
        });
		cancelButton.setOnAction((ActionEvent event) -> {
			dialog.close();
        });
	}
	
	/**
	 * Creation option for question.
	 *
	 * @param optNode the opt node
	 * @param option the option
	 * @return the h box
	 */
	private HBox creationOptionForQuestion(ObservableList<Node> optNode, MCQOption option) {
		CheckBox checkBox = new CheckBox();
		checkBox.setSelected(option.isValid());
		checkBox.setUserData(option.getId());
		TextField optTxt = new TextField(option.getOption());
		optTxt.setPrefWidth(350);
		optTxt.setTooltip(new Tooltip(option.getOption()));
		HBox textBox = new HBox(5,checkBox,optTxt);
		Button delOpt = new Button("Delete");
		Button addOpt = new Button("Add");
		HBox addDelBox = new HBox(5,addOpt,delOpt);
		HBox optHBox = new HBox(5,textBox,addDelBox);
		addOpt.setOnAction((ActionEvent event) -> {
			if(optNode.size()<6) {
				MCQOption newOption = new MCQOption();
				newOption.setOption("");
				newOption.setValid(false);
				newOption.setId(-1);
				optNode.add(creationOptionForQuestion(optNode,newOption));
			}else {
				showErrorMsg("You can add upto 6 options in a MCQuestion");
			}
		});
		delOpt.setUserData(optHBox);
		delOpt.setOnAction((ActionEvent event) -> {
			if(optNode.size()<=2) {
				showErrorMsg("MCQuestions should have atlest two options");
			}else if(showDeleteConfirmation("Are you sure want to delete the option?")) {
				Button sel = (Button)event.getSource();
				optNode.remove(sel.getUserData());
			}
		});
		return optHBox;
	}
    
    /**
     * Show delete confirmation.
     *
     * @param msg the msg
     * @return true, if successful
     */
    public boolean showDeleteConfirmation(String msg) {
	   	 Alert alert = new Alert(AlertType.CONFIRMATION,msg,ButtonType.YES, ButtonType.NO);
	   	 alert.showAndWait();
	   	 return ButtonType.YES.equals(alert.getResult());
    }
    
    /**
     * Show error msg.
     *
     * @param msg the msg
     */
    public void showErrorMsg(String msg) {
    	 Alert alert = new Alert(AlertType.ERROR,msg,ButtonType.OK);
    	 alert.show();
    }
    
    /**
     * 
     * Starts user interface
     * 
     * @param args the arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}   