/**
 * 
 */
package fr.epita.quiz.launcher;

import java.util.ArrayList;
import java.util.List;

import fr.epita.quiz.constants.Constants;
import fr.epita.quiz.datamodel.Answer;
import fr.epita.quiz.datamodel.MCQOption;
import fr.epita.quiz.datamodel.MCQuestion;
import fr.epita.quiz.datamodel.OpenQuestion;
import fr.epita.quiz.datamodel.Question;
import fr.epita.quiz.datamodel.Student;
import fr.epita.quiz.datamodel.Teacher;
import fr.epita.quiz.logger.Logger;
import fr.epita.quiz.services.QuestionDAO;
import fr.epita.quiz.services.QuizDAO;
import fr.epita.quiz.services.StudentDAO;
import fr.epita.quiz.services.TeacherDAO;

/**
 * @author bharath
 *
 */
public class Launcher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//QuizDAO quiz = new QuizDAO();
		//quiz.createQuiz();
		// TODO Auto-generated method stub
		/*Student student = new Student("bharath","Bharath11","Bharath11");
		if(student.isValidPass()) {
			StudentDAO dao = new StudentDAO();
			dao.createStudent(student);
		}*/
		/*Teacher teach = new Teacher("bharath","Bharath11","Bharath11");
		if(teach.isValidPass()) {
			TeacherDAO dao = new TeacherDAO();
			dao.createTeacher(teach);
		}*/
		/*TeacherDAO dao = new TeacherDAO();
		Teacher teach = new Teacher("bharath","Bharath11");
		System.out.println(dao.validateTeacher(teach));*/
		/*StudentDAO dao = new StudentDAO();
		Student student = new Student("bharath","Bharath12");
		System.out.println(dao.validateStudent(student));*/
		/*QuestionDAO dao = new QuestionDAO();
		List<String> topics = new ArrayList<>();
		//topics.add("Java");
		//topics.add("OOPS");
		topics.add("basic");
		//topics.add("Basic1");
		int check=2;
		if(check == Constants.MCQUESTION) {
			MCQuestion question = new MCQuestion();
			question.setQuestion("Which are the following are keywords in Java1.");
			question.setTopics(topics);
			question.setDifficulty(1);
			question.setType(Constants.MCQUESTION);
			List<MCQOption> options = new ArrayList<>();
			options.add(new MCQOption("int",true));
			options.add(new MCQOption("float",true));
			options.add(new MCQOption("String",false));
			options.add(new MCQOption("Integer",false));
			question.setOptions(options);
			dao.createMCQuestion(question);
		}else {
			OpenQuestion question = new OpenQuestion();
			question.setQuestion("What is diamond problem");
			question.setTopics(topics);
			question.setDifficulty(1);
			question.setType(1);
			Answer ans = new Answer();
			ans.setAnswer("The “diamond problem” is an ambiguity that can arise as a consequence of allowing multiple inheritance.");
			question.setAnswer(ans);
			dao.createOpenQuestion(question);
		}
		//question.setId(2);
		//dao.deleteQuestion(5);
		
		//dao.updateQuestionToDB(question);
		/*List<Question> questions = dao.searchQuestions(question);
		for(int i=0;i<questions.size();i++) {
			System.out.println(questions.get(i).toString());
		}*/

	}

}
