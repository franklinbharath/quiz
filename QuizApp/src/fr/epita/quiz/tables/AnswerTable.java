/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class AnswerTable contains all the column names for BR_Answers table.
 *
 * @author rishi
 */
public class AnswerTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME="BR_Answers";
	
	/** The Constant ANSWER. */
	public static final String ANSWER = "Answer";
	
	/** The Constant QUESTIONID. */
	public static final String QUESTIONID = "QuestionId";
	
	/** The Constant ID. */
	public static final String ID = "Id";
}
