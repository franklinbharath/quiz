/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class TeacherTable contains all the column names for BR_Teachers table.
 *
 * @author rishi
 */
public class TeacherTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_Teachers";
	
	/** The Constant USERNAME. */
	public static final String USERNAME = "Username";
	
	/** The Constant PASSWORD. */
	public static final String PASSWORD = "Password";
	
	/** The Constant ID. */
	public static final String ID = "Id";
}
