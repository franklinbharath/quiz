/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class QuestionTopicMapper contains all the column names for BR_QuestionTopicMapper table.
 *
 * @author bharath
 */
public class QuestionTopicMapper {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_QuestionTopicMapper";
	
	/** The Constant TOPICID. */
	public static final String TOPICID = "TopicId";
	
	/** The Constant QUESTIONID. */
	public static final String QUESTIONID = "QuestionId";
}
