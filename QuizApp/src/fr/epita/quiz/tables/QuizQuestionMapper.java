/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class QuizQuestionMapper contains all the column names for BR_QuizQuestionMapper table.
 *
 * @author bharath
 */
public class QuizQuestionMapper {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_QuizQuestionMapper";
	
	/** The Constant QUIZID. */
	public static final String QUIZID = "QuizId";
	
	/** The Constant QUESTIONID. */
	public static final String QUESTIONID = "QuestionId";
}
