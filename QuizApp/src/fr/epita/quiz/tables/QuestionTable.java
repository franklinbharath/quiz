/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class QuestionTable contains all the column names for BR_Questions table.
 *
 * @author bharath
 */
public class QuestionTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_Questions";
	
	/** The Constant QUESTION. */
	public static final String QUESTION = "Question";
	
	/** The Constant DIFFICULTY. */
	public static final String DIFFICULTY = "Difficulty";
	
	/** The Constant TYPE. */
	public static final String TYPE = "Type";
	
	/** The Constant ID. */
	public static final String ID = "Id";
}
