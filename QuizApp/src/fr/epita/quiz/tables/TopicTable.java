/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class TopicTable contains all the column names for BR_Topics table.
 *
 * @author bharath
 */
public class TopicTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_Topics";
	
	/** The Constant TOPIC. */
	public static final String TOPIC = "Topic";
	
	/** The Constant ID. */
	public static final String ID = "Id";
}
