/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class MCQOptionTable contains all the column names for BR_MCQOptions table.
 *
 * @author bharath
 */
public class MCQOptionTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME="BR_MCQOptions";
	
	/** The Constant OPTION. */
	public static final String OPTION = "Option";
	
	/** The Constant QUESTIONID. */
	public static final String QUESTIONID = "QuestionId";
	
	/** The Constant VALID. */
	public static final String VALID = "Valid";
	
	/** The Constant ID. */
	public static final String ID = "Id";
}
