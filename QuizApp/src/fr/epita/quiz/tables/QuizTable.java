/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class QuizTable contains all the column names for BR_Quiz table.
 *
 * @author rishi
 */
public class QuizTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_Quiz";
	
	/** The Constant ID. */
	public static final String ID = "Id";
	
	/** The Constant STUDENTID. */
	public static final String STUDENTID = "StudentId";
	
	/** The Constant TITLE. */
	public static final String TITLE = "Title";
}
