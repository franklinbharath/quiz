/**
 * 
 */
package fr.epita.quiz.tables;

// TODO: Auto-generated Javadoc
/**
 * The Class StudentTable contains all the column names for BR_Students table.
 *
 * @author rishi
 */
public class StudentTable {
	
	/** The Constant TABLENAME. */
	public static final String TABLENAME = "BR_Students";
	
	/** The Constant USERNAME. */
	public static final String USERNAME = "Username";
	
	/** The Constant PASSWORD. */
	public static final String PASSWORD = "Password";
	
	/** The Constant ID. */
	public static final String ID = "Id";
}
